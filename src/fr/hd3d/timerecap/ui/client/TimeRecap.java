package fr.hd3d.timerecap.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.timerecap.ui.client.controller.TimeRecapController;
import fr.hd3d.timerecap.ui.client.model.TimeRecapModel;
import fr.hd3d.timerecap.ui.client.view.TimeRecapView;


/*
 * Entry point class for Project Infos application.
 */
public class TimeRecap implements EntryPoint
{
    /** GXT event dispatcher. */
 
    private final EventDispatcher dispatcher = EventDispatcher.get();
    
    public void onModuleLoad()
    {
        @SuppressWarnings("unused")
        
        TimeRecapView view = new TimeRecapView();
        view.init();
  
        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
