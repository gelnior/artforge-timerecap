package fr.hd3d.timerecap.ui.client.constant;

import com.google.gwt.i18n.client.Constants.DefaultStringValue;
import com.google.gwt.i18n.client.LocalizableResource.Key;


public interface TimeRecapConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "TimeRecap".
   * 
   * @return translated "TimeRecap"
   */
  @DefaultStringValue("TimeRecap")
  @Key("TimeRecap")
  String TimeRecap();

  /**
   * Translated "TimeRecapConstants".
   * 
   * @return translated "TimeRecapConstants"
   */
  @DefaultStringValue("TimeRecapConstants")
  @Key("ClassName")
  String ClassName();

  /**
   * Translated "One column".
   * 
   * @return translated "One column"
   */
  @DefaultStringValue("One column")
  @Key("OneColumn")
  String OneColumn();

  /**
   * Translated "Two columns".
   * 
   * @return translated "Two columns"
   */
  @DefaultStringValue("Two columns")
  @Key("TwoColumns")
  String TwoColumns();

  /**
   * Translated "week-end".
   * 
   * @return translated "week-end"
   */
  @DefaultStringValue("week-end")
  @Key("weekEnd")
  String weekEnd();
  
  /**
   * Translated "&nbsp;&nbsp;To:".
   * 
   * @return translated "&nbsp;&nbsp;To:"
   */
  @DefaultStringValue("&nbsp;&nbsp;To:")
  @Key("To")
  String To();
  
  /**
   * Translated "Person".
   * 
   * @return translated "Person"
   */
  @DefaultStringValue("Person")
  @Key("Person")
  String Person();
  
  /**
   * Translated "&nbsp;&nbsp;From: ".
   * 
   * @return translated "&nbsp;&nbsp;From: "
   */
  @DefaultStringValue("&nbsp;&nbsp;From: ")
  @Key("From")
  String From();
  
  /**
   * Translated "AddTask".
   * 
   * @return translated "AddTask"
   */
  @DefaultStringValue("AddTask")
  @Key("AddTask")
  String AddTask();
  
  /**
   * Translated "Time Sheets".
   * 
   * @return translated "Time Sheets"
   */
  @DefaultStringValue("Time Sheets")
  @Key("TimeSheets")
  String TimeSheets();

  /**
   * Translated "<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>".
   * 
   * @return translated "<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>"
   */
  @DefaultStringValue("<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>")
  @Key("DontModifyStatus")
  String DontModifyStatus();
  
  /**
  * Translated "With comment".
  * 
  * @return translated "With comment"
  */
 @DefaultStringValue("With comment")
 @Key("withComment")
 String withComment();
 
 /**
  * Translated "No comment".
  * 
  * @return translated "No comment"
  */
 @DefaultStringValue("No comment")
 @Key("noComment")
 String noComment();
 

 /**
  * Translated "Work comments submission.".
  * 
  * @return translated "Work comments submission."
  */
 @DefaultStringValue("Work comments submission.")
 @Key("WorkCommentsSubmission")
 String WorkCommentsSubmission();
 
 /**
  * Translated "< Type your comment ... >".
  * 
  * @return translated "< Type your comment ... >"
  */
 @DefaultStringValue("< Type your comment ... >")
 @Key("TypeYourComment")
 String TypeYourComment();

 
 


}
