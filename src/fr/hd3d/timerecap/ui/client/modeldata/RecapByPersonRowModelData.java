package fr.hd3d.timerecap.ui.client.modeldata;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * RecapByPersonRowModelData 
 * 
 * @author HD3D
 */
public class RecapByPersonRowModelData extends RecordModelData
{
    /** Char representation for presence. */
    public static final String PRESENCE = "P";
    /** Char representation for week-end. */
    public static final String WEEKEND = "W";

    /** Automatically generated serial ID */
    private static final long serialVersionUID = -5936580785618368754L;

    /** The person concerned by the absence. */
    private PersonModelData person;
    
    private long total;

    /**
     * Default constructor, set a person in the model data (its full name) with no presence informations.
     * 
     * @param person
     *            The person concerned by the absence.
     */
    public RecapByPersonRowModelData(PersonModelData person)
    {
        super(person.getLastName() + " " + person.getFirstName() + " - " + person.getLogin());

        this.person = person;
        this.total = 0L;
    }

    /**
     * @return The person concerned by the absence.
     */
    public PersonModelData getPerson()
    {
        return person;
    }
    
    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    /**
     * Set the person concerned by the absence.
     * 
     * @param person
     *            The person concerned by the absence.
     */
    public void setPerson(PersonModelData person)
    {
        this.person = person;
    }
    
    /**
     * Create a week-end key-value (date-W) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an absence.
     */
    public void addWeekend(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, WEEKEND);
    }

    /**
     * Create a presence key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an absence.
     */
    public void addPresence(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, PRESENCE);
    }

    /**
     * Create a key-value (date-P) for the all dates between <i>start</i> and <i>end</i>.
     * 
     * @param start
     *            The first date on which set a presence.
     * @param end
     *            The last date on which set a presence.
     */
    public void addKeyValue(Date start, Date end)
    {
        DateWrapper beginDate = new DateWrapper(start);
        DateWrapper endDate = new DateWrapper(end);

        while (beginDate.before(endDate.addDays(1)))
        {
            int dayInWeek = beginDate.getDayInWeek();
            if (dayInWeek != 0 && dayInWeek != 6)
            {
                this.addPresence(beginDate);
            }
            else
            {
                this.addWeekend(beginDate);
            }
            beginDate = beginDate.addDays(1);
        }
    }

    
    /**
     * Create an duration key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an duration.
     */
    public void addDuration(Date _date, long _duration)
    {
        DateWrapper date = new DateWrapper(_date);
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, String.valueOf(_duration));
        
    }  

    /**
     * Update all day date from the template given in parameter.
     * 
     * @param template
     *            Row template.
     */
    public void updateFromTemplate(RecapByPersonRowModelData template)
    {
        this.map.clear();
        this.map.putAll(template.getProperties());
        this.setName(person.getLastName() + " " + person.getFirstName() + " - " + person.getLogin());
    }

}
