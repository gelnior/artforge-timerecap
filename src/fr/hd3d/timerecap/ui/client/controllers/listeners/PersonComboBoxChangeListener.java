package fr.hd3d.timerecap.ui.client.controllers.listeners;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityEvents;


public class PersonComboBoxChangeListener extends SelectionChangedListener<PersonModelData>
{
    @Override
    public void selectionChanged(SelectionChangedEvent<PersonModelData> se)
    {
        AppEvent event = new AppEvent(ActivityEvents.SELECTOR_PERSON_CHANGED, se.getSelectedItem());
        EventDispatcher.forwardEvent(event);
    }
}
