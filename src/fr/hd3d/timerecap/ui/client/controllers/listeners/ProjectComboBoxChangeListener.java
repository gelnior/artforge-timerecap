package fr.hd3d.timerecap.ui.client.controllers.listeners;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;


public class ProjectComboBoxChangeListener extends SelectionChangedListener<ProjectModelData>
{
    @Override
    public void selectionChanged(SelectionChangedEvent<ProjectModelData> se)
    {
        AppEvent event = new AppEvent(TimeRecapEvents.PROJECT_LOADED, se.getSelectedItem());
        EventDispatcher.forwardEvent(event);
    }
}
