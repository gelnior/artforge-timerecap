package fr.hd3d.timerecap.ui.client.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.store.GroupingStore;
import com.extjs.gxt.ui.client.store.ListStore;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.listener.EventLoadListener;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PersonDayReader;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxy;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.modeldata.RecapByPersonRowModelData;


/**
 * Model handling activity data : person list, events and activities.
 * 
 * @author HD3D
 */
public class RecapByPersonModel
{
    /** Store containing persons to display in the activity grid. */
    private final ListStore<PersonModelData> persons;
    /** Store containing personDays to display in the activity grid. */
    // private final ListStore<PersonDayModelData> personDays;
    /** Activity row generated for the activity grid. */
    private final GroupingStore<RecapByPersonRowModelData> activityRows = new GroupingStore<RecapByPersonRowModelData>();

    /** Reader used for person data parsing. */
    private final PersonReader reader = new PersonReader();
    /** Proxy used for retrieving person list. */
    private final ServicesProxy<PersonModelData> personProxy = new ServicesProxy<PersonModelData>(reader);
    /** Loader used for retrieving person list. */
    private final BaseListLoader<ListLoadResult<PersonModelData>> personLoader;

    /** Proxy used for retrieving event list. */
    private final ServicesProxy<PersonDayModelData> personDaysProxy = new ServicesProxy<PersonDayModelData>(
            new PersonDayReader());
    /** Loader used for retrieving event list. */
    // private final BaseListLoader<ListLoadResult<PersonDayModelData>> personDayLoader;
    private final ServiceStore<PersonDayModelData> serviceStore = new ServiceStore<PersonDayModelData>(
            new PersonDayReader());

    private final LogicConstraint filterDatePerson = new LogicConstraint(EConstraintLogicalOperator.AND);
    private final LogicConstraint filterProjectDatePerson = new LogicConstraint(EConstraintLogicalOperator.AND);
    private final LogicConstraint allFilter = new LogicConstraint(EConstraintLogicalOperator.AND);
    private Constraint datesFilter;
    private final Constraint personsFilter = new Constraint();
    private final Constraint activitiesFilter = new Constraint(EConstraintOperator.isnotnull, "activities", null);
    private final Constraint projectFilter = new Constraint();

    private ProjectModelData currentProject = null;

    /**
     * Default constructor, refresh person store and forwards CALENDER_PERSONS_LOADED event when data are loaded.
     */
    public RecapByPersonModel()
    {
        personLoader = new BaseListLoader<ListLoadResult<PersonModelData>>(personProxy, reader);
        persons = new ListStore<PersonModelData>(personLoader);
        personLoader.addLoadListener(new EventLoadListener(TimeRecapEvents.CALENDER_PERSONS_LOADED));

        serviceStore.addLoadListener(new AllLoadListener<PersonDayModelData>(serviceStore,
                TimeRecapEvents.CALENDER_PERSONDAYS_LOADED));
    }

    /**
     * @return Store containing all activity rows.
     */
    public GroupingStore<RecapByPersonRowModelData> getActivityListStore()
    {
        return this.activityRows;
    }

    /**
     * Set person proxy path.
     * 
     * @param path
     *            The path to set in the person proxy.
     */
    public void setPersonPath(String path)
    {
        this.personProxy.setPath(path);
    }

    public void addOnePerson(PersonModelData person)
    {
        this.persons.removeAll();
        this.persons.add(person);
    }

    /**
     * Load persons in the data store.
     */
    public void loadPersons()
    {
        this.personLoader.load();
    }
    
   
    public boolean personsIsEmpty()
    {
        return (this.persons.getCount() == 0);
    }
    

    /**
     * Load all activities where its start date is between starDate and endDate or its end date is between startDate and
     * endDate.
     * 
     * @param startDate
     *            Lower limit for events start date and end date.
     * @param endDate
     *            Upper limit for events start date and end date.
     */
    public void loadActivities(Date startDate, Date endDate, ProjectModelData project)
    {
        this.personDaysProxy.clearParameters();

        List<Long> lstId = new ArrayList<Long>();
        for (int i = 0; i < this.persons.getCount(); ++i)
        {
            PersonModelData person = persons.getAt(i);
            lstId.add(person.getId());
        }

        this.personsFilter.setColumn("person.id");
        this.personsFilter.setOperator(EConstraintOperator.in);
        this.personsFilter.setLeftMember(lstId);

        if (startDate != null && endDate != null && !lstId.isEmpty())
        {
            if (datesFilter == null)
            {
                datesFilter = new Constraint(EConstraintOperator.btw, "date", DateFormat.TIMESTAMP_FORMAT
                        .format(startDate), DateFormat.TIMESTAMP_FORMAT.format(endDate));
            }
            else
            {
                datesFilter.setLeftMember(DateFormat.TIMESTAMP_FORMAT.format(startDate));
                datesFilter.setRightMember(DateFormat.TIMESTAMP_FORMAT.format(endDate));
            }
        }
        else
        {
            return;
        }

        this.filterDatePerson.setRightMember(activitiesFilter);
        this.filterDatePerson.setLeftMember(personsFilter);
        this.currentProject = project;

        // TODO contraint activities.project.id Ne marche pas
        project = null;

        if (project != null && project.getId() != -1)
        {
            this.currentProject = project;
            this.projectFilter.setColumn("activities.project.id");
            this.projectFilter.setOperator(EConstraintOperator.eq);
            this.projectFilter.setLeftMember(project.getId());

            this.filterProjectDatePerson.setLeftMember(filterDatePerson);
            this.filterProjectDatePerson.setRightMember(datesFilter);

            this.allFilter.setLeftMember(filterProjectDatePerson);
            this.allFilter.setRightMember(projectFilter);
        }
        else
        {
            this.allFilter.setLeftMember(filterDatePerson);
            this.allFilter.setRightMember(datesFilter);
        }

        this.serviceStore.clearFilters();
        this.serviceStore.clearParameters();
        this.serviceStore.addParameter(allFilter);
        this.serviceStore.reload();
    }

    /**
     * Clear activity row store.
     */
    public void clearActivities()
    {
        this.activityRows.removeAll();
    }

    /**
     * Refresh activities row store from startDate to endDate. First, it builds a template row where all weekend are
     * set. Then a row based on template is built for each person in person store and rows are updated with activity
     * data.
     * 
     * @param startDate
     *            First day of the created rows.
     * @param endDate
     *            End day of the created rows.
     */
    public void refreshActivities(Date startDate, Date endDate, Boolean withEmptyLines)
    {
        HashMap<Long, RecapByPersonRowModelData> personRows = new HashMap<Long, RecapByPersonRowModelData>();

        this.clearActivities();
        RecapByPersonRowModelData template = this.getRowTemplate(startDate, endDate);
        this.updateDuration(personRows, template, withEmptyLines);
    }

    /**
     * Build one row with no activity for each person in person store and fill a map to access to a row via the person
     * ID.
     * 
     * @param template
     *            Row template.
     * @param personRows
     *            A map associating a person id (key) to an activity row (value).
     */
    private void buildEmptyRows(RecapByPersonRowModelData template, HashMap<Long, RecapByPersonRowModelData> personRows)
    {

        for (PersonModelData person : persons.getModels())
        {
            RecapByPersonRowModelData row = new RecapByPersonRowModelData(person);

            row.updateFromTemplate(template);

            this.activityRows.add(row);
            personRows.put(person.getId(), row);
        }
    }

    /**
     * Build a row template containing presence from startDate to endDate, week-ends.
     * 
     * @param startDate
     *            The first day of the row.
     * @param endDate
     *            The last day of the row.
     * @return The created template.
     */
    private RecapByPersonRowModelData getRowTemplate(Date startDate, Date endDate)
    {
        RecapByPersonRowModelData template = new RecapByPersonRowModelData(new PersonModelData());
        template.addKeyValue(startDate, endDate);
        return template;
    }

    /**
     * For each activity in activity store, it updates the concerned row with new activity value
     * 
     * @param personRows
     *            A map associating a person id (key) to an activity row (value).
     */
    private void updateDuration(HashMap<Long, RecapByPersonRowModelData> personRows,
            RecapByPersonRowModelData template, Boolean withEmptyLines)
    {
        for (PersonModelData currentPerson : persons.getModels())
        {
            RecapByPersonRowModelData row = new RecapByPersonRowModelData(currentPerson);
            row.updateFromTemplate(template);
            this.activityRows.add(row);
            personRows.put(currentPerson.getId(), row);
        }

        for (PersonDayModelData personDay : serviceStore.getModels())
        {
            PersonModelData person = null;
            RecapByPersonRowModelData row = null;

            if (personRows.get(personDay.getPersonId()) == null)
            {
                for (PersonModelData currentPerson : persons.getModels())
                {
                    if (currentPerson.getId().compareTo(personDay.getPersonId()) == 0)
                    {
                        person = currentPerson;
                        break;
                    }
                }
                if (person == null)
                {
                    continue;
                }
            }
            else
            {
                row = personRows.get(personDay.getPersonId());
            }

            if (row != null)
            {
                List<JSONObject> activities = personDay.getActivities();
                if (activities != null)
                {
                    long duration = 0L;
                    for (int i = 0; i < activities.size(); i++)
                    {
                        if (activities.get(i) != null)
                        {
                            JSONObject md = activities.get(i);
                            JSONValue obj = md.get("duration");
                            if (this.currentProject != null && this.currentProject.getId() != -1)
                            {
                                JSONValue projectObj = md.get("projectID");
                                if (this.currentProject.getId() == Long.parseLong((projectObj).toString()))
                                {
                                    duration = duration + Long.parseLong((obj).toString());
                                }
                            }
                            else
                            {
                                duration = duration + Long.parseLong((obj).toString());
                            }
                        }
                    }
                    row.addDuration(personDay.getDate(), duration);
                    row.setTotal(row.getTotal() + duration);
                }
            }
        }

        for (RecapByPersonRowModelData row : personRows.values())
        {
            if (withEmptyLines || row.getTotal() > 0)
            {
                this.activityRows.update(row);
            }
            else
            {
                this.activityRows.remove(row);
            }
        }
        this.activityRows.isGroupOnSort();
        this.activityRows.sort("name", SortDir.ASC);
    }
}
