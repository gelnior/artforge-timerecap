package fr.hd3d.timerecap.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;


public class TimeRecapFilter implements StoreFilter<ProjectModelData>
{
    private final List<String> statuses = new ArrayList<String>();

    public TimeRecapFilter()
    {}

    public void addStatus(String status)
    {
        statuses.add(status);
    }

    public void removeStatus(String status)
    {
        statuses.remove(status);
    }

    public boolean select(Store<ProjectModelData> store, ProjectModelData parent, ProjectModelData item, String property)
    {
        if (CollectionUtils.isEmpty(statuses))
        {
            return true;
        }
        else
        {
            for (String status : statuses)
            {
                if (status.equals(item.getStatus()))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
