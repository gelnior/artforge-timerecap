package fr.hd3d.timerecap.ui.client.model;

import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PagingSimpleActivityReader;
import fr.hd3d.common.ui.client.modeldata.reader.PagingTaskActivityReader;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxy;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Model handling TimeRecap data : person list, events and TimeRecaps.
 * 
 * @author HD3D
 */
public class TimeRecapModel extends MainModel
{

    /** Store containing activities to display in the TimeRecap grid. */
    private final ListStore<TaskActivityModelData> taskActivities;

    /** Proxy used for retrieving TimeRecap list. */
    private final ServicesPagingProxy<TaskActivityModelData> taskActivitiesProxy = new ServicesPagingProxy<TaskActivityModelData>();
    private final BasePagingLoader<PagingLoadResult<TaskActivityModelData>> taskActivityLoader;

    private final ListStore<SimpleActivityModelData> simpleActivities;

    /** Proxy used for retrieving TimeRecap list. */
    private final ServicesPagingProxy<SimpleActivityModelData> simpleActivitiesProxy = new ServicesPagingProxy<SimpleActivityModelData>();
    private final BasePagingLoader<PagingLoadResult<SimpleActivityModelData>> simpleActivityLoader;

    private LogicConstraint myFilterGroupTask;
    private LogicConstraint myFilterGroupSimple;

    private final LogicConstraint myFilterGroupEmptyTask = new LogicConstraint(EConstraintLogicalOperator.AND);
    private final LogicConstraint myFilterGroupEmptySimple = new LogicConstraint(EConstraintLogicalOperator.AND);

    private final LogicConstraint myFilterGroupPersonTask = new LogicConstraint(EConstraintLogicalOperator.AND);
    private final LogicConstraint myFilterGroupPersonSimple = new LogicConstraint(EConstraintLogicalOperator.AND);

    private final Constraint constraint = new Constraint(EConstraintOperator.neq, "duration", 0);

    private Long projectId;

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    /**
     * Default constructor, refresh person store and forwards TimeRecap_PERSONS_LOADED event when data are loaded.
     */
    public TimeRecapModel()
    {
        taskActivityLoader = new BasePagingLoader<PagingLoadResult<TaskActivityModelData>>(taskActivitiesProxy,
                new PagingTaskActivityReader());
        taskActivities = new ListStore<TaskActivityModelData>(taskActivityLoader);
        this.taskActivitiesProxy.setPath(ServicesPath.TASK_ACTIVITIES);

        simpleActivityLoader = new BasePagingLoader<PagingLoadResult<SimpleActivityModelData>>(simpleActivitiesProxy,
                new PagingSimpleActivityReader());
        simpleActivities = new ListStore<SimpleActivityModelData>(simpleActivityLoader);
        this.simpleActivitiesProxy.setPath(ServicesPath.SIMPLE_ACTIVITIES);
    }

    public ListStore<TaskActivityModelData> getTaskActivitiesStore()
    {
        return this.taskActivities;
    }

    public ListStore<SimpleActivityModelData> getSimpleActivitiesListStore()
    {
        return this.simpleActivities;
    }

    public void setActivityPath(String path)
    {
        this.taskActivitiesProxy.setPath(path);
        this.simpleActivitiesProxy.setPath(path);
    }

    public void createFilterGroupTask()
    {
        if (myFilterGroupTask == null)
        {
            myFilterGroupTask = new LogicConstraint();
            myFilterGroupTask.setOperator(EConstraintLogicalOperator.AND);
        }
    }

    public LogicConstraint getFilterTask()
    {
        createFilterGroupTask();
        return this.myFilterGroupTask;
    }

    public void createFilterGroupSimple()
    {
        if (myFilterGroupSimple == null)
        {
            myFilterGroupSimple = new LogicConstraint();
            myFilterGroupSimple.setOperator(EConstraintLogicalOperator.AND);
        }
    }

    public LogicConstraint getFilterSimple()
    {
        createFilterGroupSimple();
        return this.myFilterGroupSimple;
    }

    public LogicConstraint getFilterGroupPersonSimple()
    {
        return this.myFilterGroupPersonSimple;
    }

    public LogicConstraint getFilterGroupPersonTask()
    {
        return this.myFilterGroupPersonTask;
    }

    public void addConstraints()
    {
        myFilterGroupEmptyTask.setLeftMember(constraint);
        myFilterGroupEmptyTask.setRightMember(getFilterTask());

        myFilterGroupPersonTask.setLeftMember(myFilterGroupEmptyTask);

        myFilterGroupEmptySimple.setLeftMember(constraint);
        myFilterGroupEmptySimple.setRightMember(getFilterSimple());

        myFilterGroupPersonSimple.setLeftMember(myFilterGroupEmptySimple);

        this.taskActivitiesProxy.getParameters().add(myFilterGroupPersonTask);
        this.simpleActivitiesProxy.getParameters().add(myFilterGroupPersonSimple);
    }

    public void removeConstraints()
    {
        this.taskActivitiesProxy.getParameters().remove(myFilterGroupPersonTask);
        this.simpleActivitiesProxy.getParameters().remove(myFilterGroupPersonSimple);
    }

    public void setStorePath(ProjectModelData project)
    {
        if (project != null)
        {
            this.taskActivitiesProxy.setPath(ServicesPath.PROJECTS + project.getId() + "/"
                    + ServicesPath.TASK_ACTIVITIES);
            this.simpleActivitiesProxy.setPath(ServicesPath.PROJECTS + project.getId() + "/"
                    + ServicesPath.SIMPLE_ACTIVITIES);
        }
        else
        {
            this.taskActivitiesProxy.setPath(ServicesPath.TASK_ACTIVITIES);
            this.simpleActivitiesProxy.setPath(ServicesPath.SIMPLE_ACTIVITIES);
        }
    }

}
