package fr.hd3d.timerecap.ui.client.portlet.activity;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;

public interface IActivityPortlet
{

    void initWidgets();

    void enableNextButton();

    void disableNextButton();

    void updateDayTitle();

    void removeTaskDirty();

    void removeSimpleDirty();

    void displayTaskSelector();

    void disablePreviousButton();

    void enablePreviousButton();

    void showGridHeaders();
    
    void openTaskComment(TaskModelData task);

    void hideGridHeaders();
    
 //   void changePerson(PersonModelData person);

}
