package fr.hd3d.timerecap.ui.client.portlet.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.reader.SimpleActivityReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskActivityReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.timerecap.ui.client.portlet.activity.model.EndDateParameter;
import fr.hd3d.timerecap.ui.client.portlet.activity.model.MonthTotalCallback;
import fr.hd3d.timerecap.ui.client.portlet.activity.model.StartDateParameter;
import fr.hd3d.timerecap.ui.client.portlet.activity.model.WeekTotalCallback;
import fr.hd3d.timerecap.ui.client.view.renderer.DatetimeUtil;


/**
 * Model containing data store.
 * 
 * @author HD3D
 */
public class ActivityModel
{
    /** The store containing tasks associated to activities. */
    protected ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(new TaskReader());

    /** The store containing task activities. */
    protected ServiceStore<TaskActivityModelData> taskActivityStore = new ServiceStore<TaskActivityModelData>(
            new TaskActivityReader());
    /** The store containing simple activities. */
    protected ServiceStore<SimpleActivityModelData> simpleActivityStore = new ServiceStore<SimpleActivityModelData>(
            new SimpleActivityReader());

    /** Start date constraint for task store. */
    protected Constraint taskStartConstraint = new Constraint(EConstraintOperator.leq, TaskModelData.START_DATE_FIELD,
            null);
    /** End date constraint for task store. */
    protected Constraint taskEndConstraint = new Constraint(EConstraintOperator.geq, TaskModelData.END_DATE_FIELD, null);

    /** The constraint to retrieve only activities for the current day. */
    private final Constraint dayConstraint = new Constraint(EConstraintOperator.eq, "day.id", null);

    private FastMap<TaskModelData> taskMapById = new FastMap<TaskModelData>();

    /** True if today filter is on. */
    private boolean isFilterOn;
    /** Filter to display only activity planned for the current day. */
    private final ActivityStoreFilter todayFilter = new ActivityStoreFilter();

    /**
     * Every activities are linked to a day. Current day represents the day on which displayed activities are
     * associated.
     */
    private PersonDayModelData currentDay;

    private PersonModelData currentPerson;

    private Constraints constraints;

    private Long weekTotal = 0L;

    private Long monthTotal = 0L;

    /**
     * Default constructor. Set grouping on task activity grid.
     */
    public ActivityModel()
    {
        this.taskActivityStore.groupBy(TaskActivityModelData.WORK_OBJECT_PARENTS_NAME_FIELD);
    }

    /**
     * @return Store which contains task activity data.
     */
    public ServiceStore<TaskActivityModelData> getTaskActivityStore()
    {
        return taskActivityStore;
    }

    /**
     * @return Store which contains simple activity data.
     */
    public ServiceStore<SimpleActivityModelData> getSimpleActivityStore()
    {
        return simpleActivityStore;
    }

    /**
     * @return Store which contains task activity data.
     */
    public ServiceStore<TaskModelData> getTaskStore()
    {
        return taskStore;
    }

    /**
     * @return Current day of displayed activities.
     */
    public PersonDayModelData getCurrentDay()
    {
        return this.currentDay;
    }

    /**
     * Set the current day for selecting which activities should be displayed.
     * 
     * @param day
     *            Day to set as current day.
     */
    public void setCurrentDay(PersonDayModelData day)
    {
        this.currentDay = day;
    }

    public PersonModelData getCurrentPerson()
    {
        return currentPerson;
    }

    public void setCurrentPerson(PersonModelData currentPerson)
    {
        this.currentPerson = currentPerson;
    }

    /**
     * Set the total of work month time.
     * 
     * @param total
     *            the total of work month time.
     */

    public void setMonthTotal(Long total)
    {
        this.monthTotal = total;

    }

    /**
     * Get the total of work month time.
     * 
     * @return the total of work month time.
     */
    public Long getMonthTotal()
    {
        return monthTotal;
    }

    /**
     * @return True if current day filter is on.
     */
    public boolean isFilterOn()
    {
        return isFilterOn;
    }

    /**
     * @param isFilterOn
     *            the isFilterOn to set
     */
    public void setFilterOn(boolean isFilterOn)
    {
        this.isFilterOn = isFilterOn;
    }

    /**
     * Sets right URL for services proxy : path and parameters. Task that are retrieved are all work in progress task
     * (i.e. task status not equal to OK or closed) and assigned to currently connected user.
     */
    public void initStoreParameters()
    {
        if (this.currentPerson == null)
        {
            this.currentPerson = MainModel.currentUser;
        }
        List<String> banStatus = new ArrayList<String>();
        banStatus.add(ETaskStatus.CANCELLED.toString());
        banStatus.add(ETaskStatus.CLOSE.toString());
        banStatus.add(ETaskStatus.OK.toString());

        constraints = new Constraints();
        Constraint workerConstraint = new Constraint(EConstraintOperator.eq, TaskModelData.WORKER_ID,
                this.currentPerson.getId());
        Constraint statusConstraint = new Constraint(EConstraintOperator.notin, TaskModelData.STATUS_FIELD, banStatus);

        constraints.add(statusConstraint);
        constraints.add(workerConstraint);

        this.taskStore.addParameter(constraints);
        this.taskStore.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
        this.taskStore.addEventLoadListener(ActivityEvents.TASKS_LOADED);

        this.taskActivityStore.setPath(this.currentPerson.getDefaultPath() + "/" + ServicesPath.TASK_ACTIVITIES);
        this.taskActivityStore.addEventLoadListener(ActivityEvents.TASK_ACTIVITIES_LOADED);
        this.taskActivityStore.addParameter(dayConstraint);

        this.simpleActivityStore.setPath(this.currentPerson.getDefaultPath() + "/" + ServicesPath.SIMPLE_ACTIVITIES);
        this.simpleActivityStore.addParameter(dayConstraint);
        this.simpleActivityStore.addParameter(new Constraint(EConstraintOperator.isnotnull, "worker.id", null));
        this.simpleActivityStore.addEventLoadListener(ActivityEvents.SIMPLE_ACTIVITIES_LOADED);
    }

    /**
     * Update task IDs list used for retrieving data.
     */

    public void updateTaskIds()
    {
        taskMapById = new FastMap<TaskModelData>();
        for (TaskModelData task : taskStore.getModels())
        {
            taskMapById.put(task.getId().toString(), task);
        }
    }

    /**
     * Update current day constraint by setting day ID as a parameter. Update task store constraints with day date.
     */
    public void updateDayId()
    {
        List<String> banStatus = new ArrayList<String>();
        banStatus.add(ETaskStatus.CANCELLED.toString());
        banStatus.add(ETaskStatus.CLOSE.toString());
        banStatus.add(ETaskStatus.OK.toString());

        this.taskStore.removeParameter(constraints);
        constraints = new Constraints();
        Constraint workerConstraint = new Constraint(EConstraintOperator.eq, TaskModelData.WORKER_ID,
                this.currentPerson.getId());
        Constraint statusConstraint = new Constraint(EConstraintOperator.notin, TaskModelData.STATUS_FIELD, banStatus);

        constraints.add(statusConstraint);
        constraints.add(workerConstraint);

        this.taskStore.addParameter(constraints);

        this.simpleActivityStore.setPath(this.currentPerson.getDefaultPath() + "/" + ServicesPath.SIMPLE_ACTIVITIES);
        this.taskActivityStore.setPath(this.currentPerson.getDefaultPath() + "/" + ServicesPath.TASK_ACTIVITIES);

        this.dayConstraint.setLeftMember(currentDay.getId());

        DateWrapper date = new DateWrapper(currentDay.getDate());
        this.taskEndConstraint.setLeftMember(date.clearTime().asDate());
        this.taskStartConstraint.setLeftMember(date.clearTime().addDays(1).asDate());
    }

    /**
     * @return Total number of hours registered for displayed activities.
     */
    public int getHoursTotal()
    {
        Double hours = 0D;

        // Total number of task activity hours.
        for (TaskActivityModelData activity : this.taskActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                hours += new Double(activity.getDuration());
        }

        // Total number of simple activity hours.
        for (SimpleActivityModelData activity : this.simpleActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                hours += new Double(activity.getDuration());
        }

        return new Double(hours / (3600)).intValue();
    }

    /**
     * @return Total number of hours registered for displayed activities.
     */
    public int getSecondsTotal()
    {
        Long seconds = 0L;

        // Total number of task activity hours.
        for (TaskActivityModelData activity : this.taskActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                seconds += activity.getDuration();
        }

        // Total number of simple activity hours.
        for (SimpleActivityModelData activity : this.simpleActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                seconds += activity.getDuration();
        }

        return seconds.intValue();
    }

    /**
     * Get task IDs for task that are planned for today then apply store filter on task activity store that will remove
     * all activity linked to tasked that are not planned today.
     */
    public void addTodayFilter()
    {
        this.setFilterOn(true);
        this.todayFilter.getIds().clear();
        for (TaskModelData task : taskStore.getModels())
        {
            Date startDate = task.getStartDate();
            Date endDate = task.getEndDate();
            Date currentDate = this.currentDay.getDate();
            if (startDate != null && endDate != null && (currentDate.after(startDate) || currentDate.equals(startDate))
                    && (currentDate.before(endDate) || currentDate.equals(endDate)))
            {
                this.todayFilter.getIds().add(task.getId());
            }
        }

        this.taskActivityStore.addFilter(this.todayFilter);
        this.taskActivityStore.applyFilters(TaskActivityModelData.TASK_ID_FIELD);
    }

    /**
     * Remove current day task IDs filter on task activity store.
     */
    public void removeTodayFilter()
    {
        this.setFilterOn(false);
        this.taskActivityStore.removeFilter(this.todayFilter);
        this.taskActivityStore.applyFilters(TaskActivityModelData.TASK_ID_FIELD);
    }

    /**
     * Create a new simple activity inside simple activity store. This activity is already configured for currently
     * selected day and connected user. Project, duration and type are not registered.
     */
    public void addEmptySimpleActivity()
    {
        SimpleActivityModelData activity = new SimpleActivityModelData();
        activity.setDayId(this.getCurrentDay().getId());
        activity.setFilledDate(DatetimeUtil.today());
        activity.setFilledById(this.currentPerson.getId());
        activity.setWorkerId(this.currentPerson.getId());

        this.getSimpleActivityStore().add(activity);
    }

    /**
     * Reload total of seconds spent in activities between <i>startDate</i> and <i>endDate</i> for current user.
     * 
     * @param startDate
     *            Start date of time area on which total will be calculated.
     * @param endDate
     *            End date of time area on which total will be calculated.
     */
    public void reloadActivityWeekTotal(Date startDate, Date endDate)
    {
        WeekTotalCallback callback = new WeekTotalCallback(this.currentPerson.getId());
        String path = ServicesPath.PERSONS + this.currentPerson.getId() + "/" + ServicesPath.ACTIVITIES
                + ServicesPath.DURATION;

        StartDateParameter startDateParameter = new StartDateParameter(startDate);
        EndDateParameter endDateParameter = new EndDateParameter(endDate);
        path += ParameterBuilder.parametersToString(Arrays.asList(startDateParameter, endDateParameter));
        RestRequestHandlerSingleton.getInstance().getRequest(path, callback);
    }

    public void setWeekTotal(Long total)
    {
        this.weekTotal = total;
    }

    public Long getWeekTotal()
    {
        return this.weekTotal;
    }

    public FastMap<TaskModelData> getTaskMapById()
    {
        return taskMapById;
    }

    /**
     * Return the task concerning to the given activity.
     * 
     * @param activity
     *            the given activity.
     * @return the task
     */
    public TaskModelData getTaskForActivity(TaskActivityModelData activity)
    {
        if (activity == null)
            return null;
        return taskMapById.get(activity.getTaskId().toString());
    }

    /**
     * Return if the activity owner is the associated task owner.
     * 
     * @param activity
     *            the given activity
     * @return if it is the associated task owner.
     */
    public boolean isOwnTask(TaskActivityModelData activity)
    {

        return (taskMapById.get(activity.getTaskId().toString()) == null);
    }

    /**
     * Reload total of seconds spent in activities between <i>startDate</i> and <i>endDate</i> for current user.
     * 
     * @param startDate
     *            Start date of time area on which total will be calculated.
     * @param endDate
     *            End date of time area on which total will be calculated.
     */
    public void reloadActivityMonthTotal(Date startDate, Date endDate)
    {
        MonthTotalCallback callback = new MonthTotalCallback(this.currentPerson.getId());
        String path = ServicesPath.PERSONS + this.currentPerson.getId() + "/" + ServicesPath.ACTIVITIES
                + ServicesPath.DURATION;

        StartDateParameter startDateParameter = new StartDateParameter(startDate);
        EndDateParameter endDateParameter = new EndDateParameter(endDate);
        path += ParameterBuilder.parametersToString(Arrays.asList(startDateParameter, endDateParameter));
        RestRequestHandlerSingleton.getInstance().getRequest(path, callback);
    }

}
