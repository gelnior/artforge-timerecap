package fr.hd3d.timerecap.ui.client.portlet.activity.widget;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


/**
 * A combo box to select an activity simple type.
 * 
 * @author HD3D
 */
public class SimpleTypeComboBox extends FieldComboBox
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor. Set available values inside combobox.
     */
    public SimpleTypeComboBox()
    {
        this.setValueField(FieldModelData.VALUE_FIELD);

        this.getStore().add(new FieldModelData(1, COMMON_CONSTANTS.Meeting(), ESimpleActivityType.MEETING.toString()));
        this.getStore().add(
                new FieldModelData(2, COMMON_CONSTANTS.TechnicalIncident(), ESimpleActivityType.TECHNICAL_INCIDENT
                        .toString()));
        this.getStore().add(new FieldModelData(3, COMMON_CONSTANTS.Other(), ESimpleActivityType.OTHER.toString()));
    }
}
