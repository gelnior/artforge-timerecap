package fr.hd3d.timerecap.ui.client.portlet.activity.widget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityEvents;


public class TaskSimpleExplorerController extends SimpleExplorerController<TaskModelData>
{
    private final TaskSimpleExplorer panel;

    private final Constraint projectConstraint = new Constraint(EConstraintOperator.eq, "project.id", null);
    private final Constraint taskTypeConstraint = new Constraint(EConstraintOperator.eq, "taskType.id", null);
    private final LuceneConstraint workObjectConstraint = new LuceneConstraint(EConstraintOperator.prefix, "wo_name",
            "", null);

    private final LogicConstraint workerConstraint = new LogicConstraint(EConstraintLogicalOperator.OR, new Constraint(
            EConstraintOperator.neq, "worker.id", MainModel.currentUser.getId()), new Constraint(
            EConstraintOperator.isnull, "worker.id"));

    public TaskSimpleExplorerController(SimpleExplorerModel<TaskModelData> model, TaskSimpleExplorer view)
    {
        super(model, view);

        this.panel = view;

        this.buildConstraints();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(ActivityEvents.SELECTOR_PROJECT_CHANGED);
        this.registerEventTypes(ActivityEvents.SELECTOR_TASK_TYPE_CHANGED);
        this.registerEventTypes(ActivityEvents.KEY_UP_WORKOBJECT_FILTER);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (this.isMasked)
        {
            this.forwardToChild(event);
        }
        else if (type == ActivityEvents.SELECTOR_PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == ActivityEvents.SELECTOR_TASK_TYPE_CHANGED)
        {
            this.onTaskTypeChanged(event);
        }
        else if (type == ActivityEvents.KEY_UP_WORKOBJECT_FILTER)
        {
            this.onKeyUpWorkObject(event);
        }
        else
        {
            super.handleEvent(event);
        }
    }
    
    private void onKeyUpWorkObject(AppEvent event)
    {
        String rawValue = event.getData();
        if (rawValue == null)
        {
            return;
        }
        if (projectConstraint.getLeftMember() != null && taskTypeConstraint.getLeftMember() != null)
        {
            workObjectConstraint.setLeftMember(rawValue);
            this.model.getModelStore().reload();
            this.panel.enableButtons();
        }
    }   

    @Override
    protected void onAddClicked()
    {
        TaskModelData task = this.model.getNewModelInstance();
        task.setWorkerID(MainModel.currentUser.getId());
        task.setName("");
        task.setStatus(ETaskStatus.WORK_IN_PROGRESS.toString());
        task.setTaskTypeId((Long) this.taskTypeConstraint.getLeftMember());
        task.setProjectID((Long) projectConstraint.getLeftMember());
        task.setStartable(Boolean.TRUE);
        task.setStartDate(new Date());
        task.setWorkerName(MainModel.currentUser.getFullName());

        if (this.model.getModelStore() != null)
        {
            this.model.getModelStore().add(task);
            task.save();
        }
    }

    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();

        if (project != null)
        {
            this.projectConstraint.setLeftMember(project.getId());
            this.panel.setProjectFilterTaskType(project.getId());
        }

        if (this.taskTypeConstraint.getLeftMember() != null)
        {
            this.model.getModelStore().reload();
            this.panel.enableButtons();
        }

        // this.panel.reloadTrees(project);
    }

    private void onTaskTypeChanged(AppEvent event)
    {
        TaskTypeModelData taskType = event.getData();

        if (taskType != null)
        {
            this.taskTypeConstraint.setLeftMember(taskType.getId());
        }

        if (this.projectConstraint.getLeftMember() != null)
        {
            this.model.getModelStore().reload();
            this.panel.enableButtons();
        }
    }

    private void buildConstraints()
    {
        List<String> banStatus = new ArrayList<String>();
        banStatus.add(ETaskStatus.CANCELLED.toString());
        banStatus.add(ETaskStatus.CLOSE.toString());
        banStatus.add(ETaskStatus.OK.toString());
        Constraint statusConstraint = new Constraint(EConstraintOperator.notin, TaskModelData.STATUS_FIELD, banStatus);

        LogicConstraint and = new LogicConstraint(EConstraintLogicalOperator.AND, projectConstraint, taskTypeConstraint);
        LogicConstraint and_bis = new LogicConstraint(EConstraintLogicalOperator.AND, and, statusConstraint);
        LogicConstraint and_ter = new LogicConstraint(EConstraintLogicalOperator.AND, and_bis, workerConstraint);
   
        this.model.getModelStore().addParameter(and_ter);
        
        this.model.getModelStore().addParameter(workObjectConstraint);
    }
}
