package fr.hd3d.timerecap.ui.client.portlet.activity.model;

import org.restlet.client.Request;
import org.restlet.client.Response;


import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityEvents;


/**
 * Value returned by activities duration service is not an object list. So the data have to be parsed in a specific way.
 * This callback extracts the duration from returned data and forwards it to event dispatcher with an
 * ACTIVITY_WEEK_TOTAL_RETRIEVED event.
 * 
 * @author HD3D
 */
public class WeekTotalCallback extends BaseCallback
{
    
    private Long personId;
    
    public WeekTotalCallback(Long personId)
    {
       this.personId = personId;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            if(json.isEmpty()){
                return;
            }

            JSONValue object = JSONParser.parse(json);
            JSONValue resultMap = object.isObject().get("records");

            String userId =  this.personId.toString();
            Double duration = resultMap.isArray().get(0).isObject().get(userId).isNumber().doubleValue();
            Long total = duration.longValue();
            if (total == null)
            {
                total = 0L;
            }

            EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED, total);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while retrieving total week activity time.", e);
        }
    }
}
