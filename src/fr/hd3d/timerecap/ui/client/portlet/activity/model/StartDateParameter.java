package fr.hd3d.timerecap.ui.client.portlet.activity.model;

import java.util.Date;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;


/**
 * Build a start date URL parameter like "startDate=<i>date</i>".
 * 
 * @author HD3D
 */
public class StartDateParameter implements IUrlParameter
{
    /** Date to set as parameter value. */
    private final Date startDate;

    /**
     * Constructor.
     * 
     * @param startDate
     *            Date to set as parameter value.
     */
    public StartDateParameter(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Return parameter value as string.
     */
    public String toJson()
    {
        return DateFormat.DATE_TIME.format(startDate);
    }

    /**
     * @return The string to set as parameter : startDate=<i>startDate</i>.
     */
    @Override
    public String toString()
    {
        return Const.STARTDATE + "=" + toJson();
    }
}
