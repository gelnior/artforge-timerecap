package fr.hd3d.timerecap.ui.client.portlet.activity.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


public class TaskTypeEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    private final ModelDataComboBox<TaskTypeModelData> combo;

    private final String name = "";

    /** Default Constructor. */
    public TaskTypeEditor(ModelDataComboBox<TaskTypeModelData> combo)
    {
        super(combo);

        this.combo = combo;
    }

    @Override
    public Object preProcessValue(Object value)
    {
        TaskTypeModelData object;

        if (value == null)
        {
            return null;
        }
        else
        {
            String name = (String) value;

            object = combo.getValueByName(name);

            return object;
        }
    }

    @Override
    public Object postProcessValue(Object value)
    {
        TaskTypeModelData type = (TaskTypeModelData) value;

        if (value == null || (name != null && name.equals(type.getName())))
        {
            return null;
        }
        else
        {
            return type.getName();
        }
    }
}
