package fr.hd3d.timerecap.ui.client.portlet.activity.widget;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.timerecap.ui.client.constant.TimeRecapConstants;


/**
 * Task selector dialog allow user to select unassigned or uncreated tasks to set activity time on it.
 * 
 * @author HD3D
 */
public class TaskSelectorDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static TimeRecapConstants CONSTANTS = GWT.create(TimeRecapConstants.class);

    /** Simple explorer used to display available tasks. */
    private final TaskSimpleExplorer explorer = new TaskSimpleExplorer();

    /**
     * Default constructor. It registers simple explorer controller to event dispatcher and set dialog styles.
     */
    public TaskSelectorDialog()
    {
        EventDispatcher.get().addController(explorer.getController());

        this.setStyles();
        this.setExplorer();
        explorer.unIdle();
    }

    /** Un-Masks explorer controller when shown. */
    @Override
    public void show()
    {
        explorer.unIdle();
        explorer.refresh();
        super.show();
    }

    /** Masks explorer controller when hidden. */
    @Override
    public void hide()
    {
        super.hide();
        // explorer.idle();
    }

    /** Set dialog styles : layout, size... */
    private void setStyles()
    {
        this.setHeading(CONSTANTS.AddTask());
        this.setButtons("");
        this.setFrame(true);
        this.setResizable(false);
        this.setModal(true);
        // this.setSize(700, 400);
        this.setSize(500, 400);
        this.setLayout(new FitLayout());
    }

    /** Set simple explorer configuration. */
    private void setExplorer()
    {
        this.explorer.hideDeleteToolItem();
        this.explorer.setPath(ServicesPath.TASKS);
        this.explorer.setSingleSelection();
        this.add(explorer, new FitData());
    }
}
