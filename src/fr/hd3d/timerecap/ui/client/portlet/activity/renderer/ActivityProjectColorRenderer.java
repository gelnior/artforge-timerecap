package fr.hd3d.timerecap.ui.client.portlet.activity.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;


public class ActivityProjectColorRenderer implements GridCellRenderer<ActivityModelData>
{

    public Object render(ActivityModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<ActivityModelData> store, Grid<ActivityModelData> grid)
    {
        String color = model.get(property);

        return getTypeRendered(color);
    }

    private String getTypeRendered(String color)
    {
        String cellCode = "";
        if (!Util.isEmptyString(color))
        {
            cellCode = "<div style='width:100%; height:100%; text-align:center; background-color:" + color + ";'>"
                    + "&nbsp;" + "</div>";
        }
        return cellCode;
    }

}
