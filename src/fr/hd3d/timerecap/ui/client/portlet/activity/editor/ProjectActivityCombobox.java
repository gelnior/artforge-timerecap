package fr.hd3d.timerecap.ui.client.portlet.activity.editor;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProjectReader;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


/**
 * Project activity combo box allow to select a project from database. Displayed projects are prefixed with a color box
 * corresponding to project color.
 * 
 * @author HD3D
 */
public class ProjectActivityCombobox extends ModelDataComboBox<ProjectModelData>
{

    /**
     * Default constructor
     */
    public ProjectActivityCombobox()
    {
        super(new ProjectReader());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
        return  [ 
        '<tpl for=".">', 
        '<div class="x-combo-list-item">',
        ' <span style="background-color:{color}; font-size: 13px;">',
        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;',
        '<span style="font-weight: bold; font-size : 12px; text-transform: uppercase;">&nbsp;{name}&nbsp;&nbsp;</span>',
        ' </div>', 
        '</tpl>' 
        ].join("");
    }-*/;

}
