package fr.hd3d.timerecap.ui.client.portlet.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.AfterEditListener;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.ReloadListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.basic.ActivityHoursEditor;
import fr.hd3d.common.ui.client.widget.basic.BasicTaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.basic.TaskActivityTypeRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.HoursRenderer;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.grid.renderer.BigTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;
import fr.hd3d.timerecap.ui.client.constant.TimeRecapConstants;
import fr.hd3d.timerecap.ui.client.portlet.activity.editor.ProjectActivityCombobox;
import fr.hd3d.timerecap.ui.client.portlet.activity.editor.ProjectEditor;
import fr.hd3d.timerecap.ui.client.portlet.activity.renderer.SimpleActivityTypeRenderer;
import fr.hd3d.timerecap.ui.client.portlet.activity.widget.SimpleTypeComboBox;
import fr.hd3d.timerecap.ui.client.portlet.activity.widget.TaskSelectorDisplayer;
import fr.hd3d.timerecap.ui.client.view.renderer.DatetimeUtil;


/**
 * ActivityPortlet displays currently opened task assigned to the user. It allows user to set time on user activities.
 * Available activities are divided in two grids : one for planned tasks and one another for simple activies such as
 * meeting or technical incident.
 * 
 * @author HD3D
 */
public class ActivityPortlet extends BorderedPanel implements IActivityPortlet
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static TimeRecapConstants CONSTANTS = GWT.create(TimeRecapConstants.class);

    /** Model contains data. */
    protected ActivityModel model = new ActivityModel();
    /** Widget controller which handle events. */
    protected ActivityController controller = new ActivityController(this, model);

    /** The taskActivity grid. */
    protected BaseEditorGrid<TaskActivityModelData> taskActivityGrid;
    /** The simpleActivity grid. */
    protected BaseEditorGrid<SimpleActivityModelData> simpleActivityGrid;

    /** Title that displays current day and total hours. */
    protected Text dayTitle = new Text();

    /** Text that display total hours work in current week. */
    protected Text weekTitle = new Text();

    /** Text that display total hours work in current month. */
    protected Text monthTitle = new Text();

    /** Button that displays previous day on click. */
    protected Button previous = new Button("<<");
    /** Button that displays next day on click. */
    protected Button next = new Button(">>");

    /** Button that allow user to display only activities planned for current day. */
    protected ToolBarToggleButton allOrPlanned = new ToolBarToggleButton(Hd3dImages.getTodayIcon(),
            "Current day activities", ActivityEvents.ALL_OR_PLANNED_CLICKED);

    /** TextArea that containing the comment for the new approval done created. */
    private final TextArea taskCommentTextArea = new TextArea();
    /** Dialog that allowed to fill the comment for the new approval done created. */
    private Dialog taskCommentDialog = null;
    /** The default text print in the textArea for the comment of the approval */
    private final String defaultTextTaskComment = CONSTANTS.TypeYourComment();
    /** Title of dialog to fill the comment of approval. */
    private final String headingTaskCommentDialog = CONSTANTS.WorkCommentsSubmission();

    /** Text */
    protected Text personText = new Text();

    /**
     * Default constructor : initialize data and widgets.
     */
    public ActivityPortlet()
    {
        EventDispatcher.get().addController(controller);
        this.setHeaderVisible(false);
        this.setBorders(false);
    }

    public void init()
    {
        this.controller.handleEvent(ActivityEvents.INIT);
    }

    /**
     * Initialize grids, toolbar and styles.
     */
    public void initWidgets()
    {
        this.setStyles();
        this.setToolbar();
        this.setGrids();
        this.setDialog();
    }

    /**
     * Initialize dialog.
     */
    private void setDialog()
    {

        taskCommentDialog = new Dialog() {

            @Override
            protected void onButtonPressed(Button button)
            {

                super.onButtonPressed(button);
                if (button == getButtonById(CANCEL))
                {
                    EventDispatcher.forwardEvent(ActivityEvents.REFRESH_ACTIVITIES);
                    return;
                }
                TaskModelData task = getData("task");
                if (button == getButtonById(YES))
                {

                    if (task == null)
                    {
                        return;
                    }
                    task.set(TaskModelData.COMMENT_FIELD, taskCommentTextArea.getValue());

                }

                task.save();

            }

        };

        // position and size
        taskCommentDialog.setLayout(new BorderLayout());
        BorderLayoutData layoutDataTaskCommentDialog = new BorderLayoutData(LayoutRegion.CENTER);
        taskCommentDialog.setHeight(200);
        taskCommentDialog.setWidth(400);

        // button
        taskCommentDialog.yesText = CONSTANTS.withComment();
        taskCommentDialog.noText = CONSTANTS.noComment();
        taskCommentDialog.setButtons(Dialog.YESNOCANCEL);
        taskCommentDialog.setHideOnButtonClick(true);

        // default text
        taskCommentTextArea.setEmptyText(defaultTextTaskComment);

        // modal
        taskCommentDialog.setModal(true);

        // heading
        taskCommentDialog.setHeading(headingTaskCommentDialog);

        taskCommentDialog.add(taskCommentTextArea, layoutDataTaskCommentDialog);
    }

    /**
     * Update day title with current day and total hours spent during this day. Format is : dd/MM/yyyy('TotalHour'h).
     */
    public void updateDayTitle()
    {

        if (this.model.getCurrentDay() != null)
        {
            Date currentDate = this.model.getCurrentDay().getDate();

            if (currentDate != null)
            {
                StringBuilder name = new StringBuilder();
                StringBuilder title = new StringBuilder();
                StringBuilder weekWorkTime = new StringBuilder();
                StringBuilder monthWorkTime = new StringBuilder();

                title.append(DateFormat.FRENCH_DATE.format(currentDate));
                DateWrapper wrapper = new DateWrapper(currentDate);
                int dayInWeek = wrapper.getDayInWeek();
                title.append(" ").append(DatetimeUtil.getDayFromDayInWeek(dayInWeek));

                String dayTime = getStringFromTime(new Long(this.model.getSecondsTotal()));
                weekWorkTime.append("<span style='16px; margin-right: 10px;'>Current week : ")
                        .append(getStringFromTime(this.model.getWeekTotal())).append("</span>");
                monthWorkTime
                        .append("<span style=' margin-right: 10px;'>" + DatetimeUtil.getMonthString(wrapper.getMonth()))
                        .append(" : ").append(getStringFromTime(this.model.getMonthTotal())).append("</span>");
                title.append(" : <b>").append(dayTime).append("</b>&#160;&#160;&#160;");

                weekTitle.setText(weekWorkTime.toString());
                monthTitle.setText(monthWorkTime.toString());
                dayTitle.setText(title.toString());

                name.append("<b>" + this.model.getCurrentPerson().getFullName() + "</b>&#160;&#160;&#160;");
                personText.setText(name.toString());
            }
        }
    }

    private String getStringFromTime(Long value)
    {
        String time = "";

        float nbSeconds = value / 3600;
        int hours = (int) Math.floor(nbSeconds);
        value = value % 3600;

        if (value == 0)
        {
            time = hours + "h";
        }
        else if (value > 0)
        {
            time = hours + "h " + (Math.round(value / 60));
        }
        return time;
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setHeight(500);
        this.setBorders(true);

        this.setLayout(new BorderLayout());
        this.setHeading(CONSTANTS.TimeSheets());
        this.setTitle("activities");
        this.hideToolTip();

        this.dayTitle.setStyleAttribute("font-weight", "bold");
        this.dayTitle.setStyleAttribute("font-size", "15px");
        this.weekTitle.setStyleAttribute("font-size", "20px");
        this.monthTitle.setStyleAttribute("font-size", "20px");
    }

    /**
     * Add previous button, next button and day title to toolbar.
     */
    private void setToolbar()
    {
        ToolBar bar = new ToolBar();

        bar.setStyleAttribute("padding", "5px");
        this.setTopComponent(bar);
        bar.clearState();
        bar.removeAll();

        personText.setWidth(140);
        bar.add(personText);

        bar.add(previous);

        int availableDays = 300;

        if (availableDays > 1)
        {
            bar.add(previous);
            bar.add(next);
            bar.add(new SeparatorToolItem());
        }
        bar.add(new ToolBarButton(Hd3dImages.getAddIcon(), CONSTANTS.AddTask(), ActivityEvents.ADD_TASK_CLICKED));

        bar.add(this.allOrPlanned);
        Button refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh activity grids",
                ActivityEvents.REFRESH_ACTIVITIES);
        refreshButton.addSelectionListener(new ReloadListener<SimpleActivityModelData>(this.model
                .getSimpleActivityStore()));
        bar.add(refreshButton);
        bar.add(new SeparatorToolItem());
        this.dayTitle.setStyleAttribute("padding-left", "3px");
        bar.add(this.dayTitle);
        bar.add(new FillToolItem());

        bar.add(weekTitle);
        bar.add(monthTitle);
        bar.add(allOrPlanned);

        this.previous.addSelectionListener(new ButtonClickListener(ActivityEvents.PREVIOUS_CLICKED));
        this.next.addSelectionListener(new ButtonClickListener(ActivityEvents.NEXT_CLICKED));

    }

    /**
     * Remove dirty markers on edited cells for task activity grid.
     */
    public void removeTaskDirty()
    {
        this.taskActivityGrid.getStore().commitChanges();
    }

    /**
     * Remove dirty markers on edited cells for simple activity grid.
     */
    public void removeSimpleDirty()
    {
        this.simpleActivityGrid.getStore().commitChanges();
    }

    /**
     * Enable next button.
     */
    public void enableNextButton()
    {
        this.next.enable();
    }

    /**
     * Disable next button.
     */
    public void disableNextButton()
    {
        this.next.disable();
    }

    /** Enable previous day button. */
    public void enablePreviousButton()
    {
        this.previous.enable();
    }

    /** Disable previous day button. */
    public void disablePreviousButton()
    {
        this.previous.disable();
    }

    /**
     * Display a task selector to add time on unassigned or uncreated tasks.
     */
    public void displayTaskSelector()
    {
        TaskSelectorDisplayer.show();
    }

    /** @return total activity time registerd for current day in seconds. */
    public int getActivitiesTotal()
    {
        return this.model.getSecondsTotal();
    }

    public void showGridHeaders()
    {
        this.taskActivityGrid.showHeaders();
        this.simpleActivityGrid.showHeaders();
    }

    public void hideGridHeaders()
    {
        this.taskActivityGrid.hideHeaders();
        this.simpleActivityGrid.hideHeaders();
    }

    /**
     * Set grids that displays tasks assigned to currently connected user.
     */
    private void setGrids()
    {
        // Create grids.
        this.taskActivityGrid = new BaseEditorGrid<TaskActivityModelData>(this.model.getTaskActivityStore(),
                this.getTaskColumnModel(), ActivityEvents.TASK_ACTIVITY_SELECTED);
        this.simpleActivityGrid = new BaseEditorGrid<SimpleActivityModelData>(this.model.getSimpleActivityStore(),
                this.getSimpleColumnModel());

        this.taskActivityGrid.addListener(Events.BeforeEdit, new Listener<GridEvent<TaskActivityModelData>>() {

            public void handleEvent(GridEvent<TaskActivityModelData> be)
            {
                if (be.getColIndex() != be.getGrid().getColumnModel()
                        .getIndexById(TaskActivityModelData.TASK_STATUS_FIELD))
                {
                    return;
                }
                if (be.getSource() instanceof BaseEditorGrid)
                {

                    TaskActivityModelData activity = be.getModel();
                    if (model.isOwnTask(activity))
                    {

                        be.setCancelled(true);
                        MessageBox.alert(COMMON_CONSTANTS.warning(), CONSTANTS.DontModifyStatus(), null);
                    }
                }
            }
        });

        // Set Styles
        this.taskActivityGrid.setLoadMask(true);
        this.taskActivityGrid.setColumnReordering(true);
        ((GroupingView) this.taskActivityGrid.getView()).setShowGroupedColumn(false);
        this.taskActivityGrid.setAutoExpandColumn(TaskActivityModelData.WORK_OBJECT_NAME_FIELD);
        this.taskActivityGrid.setAutoExpandMax(2000);
        this.taskActivityGrid.setClicksToEdit(ClicksToEdit.TWO);

        this.simpleActivityGrid.setLoadMask(true);
        this.simpleActivityGrid.setAutoExpandColumn(ActivityModelData.COMMENT_FIELD);
        this.simpleActivityGrid.setAutoExpandMax(3000);
        this.simpleActivityGrid.setClicksToEdit(ClicksToEdit.TWO);

        // Set Context Menus
        EasyMenu menuTask = this.taskActivityGrid.getMenu();
        menuTask.removeAll();
        menuTask.addItem(CONSTANTS.AddTask(), ActivityEvents.ADD_TASK_CLICKED, Hd3dImages.getAddIcon());
        menuTask.addItem("Refresh", ActivityEvents.REFRESH_ACTIVITIES, Hd3dImages.getRefreshIcon());
        // menuTask.add(new RefreshMenuItem<TaskModelData>(this.model.getTaskStore()));

        // Set After Edit Listeners
        this.taskActivityGrid.addListener(Events.AfterEdit, new AfterEditListener<TaskActivityModelData>(
                ActivityEvents.TASK_ACTIVITY_EDITED));
        this.simpleActivityGrid.addListener(Events.AfterEdit, new AfterEditListener<SimpleActivityModelData>(
                ActivityEvents.SIMPLE_ACTIVITY_EDITED));

        // Add grids to panel.
        ContentPanel taskActivityPanel = new ContentPanel();
        taskActivityPanel.setHeading("Planned activities");
        taskActivityPanel.setLayout(new FitLayout());
        taskActivityPanel.add(taskActivityGrid);
        taskActivityPanel.setBorders(false);

        ContentPanel simpleActivityPanel = new ContentPanel();
        simpleActivityPanel.setHeading("Unplanned activities");
        simpleActivityPanel.setLayout(new FitLayout());
        simpleActivityPanel.add(simpleActivityGrid);
        simpleActivityPanel.setBorders(false);

        BorderLayoutData centerData = new BorderLayoutData(LayoutRegion.CENTER);
        centerData.setMargins(new Margins(0, 0, 0, 0));
        centerData.setSplit(true);
        centerData.setMaxSize(1000);

        BorderLayoutData southData = new BorderLayoutData(LayoutRegion.SOUTH);
        southData.setMargins(new Margins(0, 0, 0, 0));
        southData.setSize(100);
        southData.setSplit(true);
        southData.setMaxSize(400);

        this.add(taskActivityPanel, centerData);
        this.add(simpleActivityPanel, southData);

    }

    /**
     * @return Column list for simple activity grid.
     */
    private ColumnModel getSimpleColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        // Project name column
        ColumnConfig projectColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.PROJECT_NAME_FIELD,
                COMMON_CONSTANTS.Project(), 78);
        projectColumn.setRenderer(new PaddingTextRenderer<ModelData>());
        projectColumn.setEditor(new ProjectEditor(new ProjectActivityCombobox()));

        // Type column
        ColumnConfig typeColumn = GridUtils.addColumnConfig(columns, SimpleActivityModelData.TYPE_FIELD,
                COMMON_CONSTANTS.Type(), 120);
        typeColumn.setRenderer(new SimpleActivityTypeRenderer());
        typeColumn.setEditor(new FieldComboBoxEditor(new SimpleTypeComboBox()));

        // Duration column
        ColumnConfig durationColumn = GridUtils.addColumnConfig(columns, SimpleActivityModelData.DURATION_FIELD,
                COMMON_CONSTANTS.Duration(), 60);
        durationColumn.setRenderer(new HoursRenderer<ModelData>());
        durationColumn.setEditor(new FieldComboBoxEditor(new ActivityHoursEditor(12)));

        // Comment column
        ColumnConfig commentColumn = GridUtils.addColumnConfig(columns, SimpleActivityModelData.COMMENT_FIELD,
                COMMON_CONSTANTS.Comment());
        commentColumn.setEditor(new CellEditor(new TextArea()));

        return new ColumnModel(columns);
    }

    /**
     * @return Column list for task activity grid.
     */
    private ColumnModel getTaskColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        // Project name column
        ColumnConfig projectColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.PROJECT_NAME_FIELD,
                COMMON_CONSTANTS.Project(), 78);
        projectColumn.setRenderer(new PaddingTextRenderer<ModelData>());

        // Type column
        ColumnConfig typeColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.TASK_TYPE_NAME_FIELD,
                COMMON_CONSTANTS.Type(), 120);
        typeColumn.setRenderer(new TaskActivityTypeRenderer());

        // Work object parent name column
        ColumnConfig pathColumn = GridUtils.addColumnConfig(columns,
                TaskActivityModelData.WORK_OBJECT_PARENTS_NAME_FIELD, "Parent", 140);
        pathColumn.setRenderer(new PaddingTextRenderer<ModelData>());

        // Work object name column
        ColumnConfig nameColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.WORK_OBJECT_NAME_FIELD,
                COMMON_CONSTANTS.WorkObject(), 160);
        nameColumn.setRenderer(new BigTextRenderer<ModelData>());

        // Duration column
        ColumnConfig durationColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.DURATION_FIELD, "<b>"
                + COMMON_CONSTANTS.Duration() + "</b>", 59);
        durationColumn.setRenderer(new HoursRenderer<ModelData>());
        durationColumn.setEditor(new FieldComboBoxEditor(new ActivityHoursEditor(12)));

        // Status column
        ColumnConfig statusColumn = GridUtils.addColumnConfig(columns, TaskActivityModelData.TASK_STATUS_FIELD, "<b>"
                + COMMON_CONSTANTS.Status() + "</b>", 120);
        statusColumn.setRenderer(new BasicTaskStatusRenderer<Hd3dModelData>());

        // Start Date column
        ColumnConfig startDate = GridUtils.addColumnConfig(columns, TaskActivityModelData.TASK_START_DATE_FIELD,
                COMMON_CONSTANTS.StartDate(), 65);
        startDate.setAlignment(HorizontalAlignment.CENTER);
        startDate.setRenderer(new PaddingTextRenderer<ModelData>());
        startDate.setDateTimeFormat(DateFormat.FRENCH_DATE);

        // End Date column
        ColumnConfig endDate = GridUtils.addColumnConfig(columns, TaskActivityModelData.TASK_END_DATE_FIELD,
                COMMON_CONSTANTS.EndDate(), 65);
        endDate.setAlignment(HorizontalAlignment.CENTER);
        endDate.setRenderer(new PaddingTextRenderer<ModelData>());
        endDate.setDateTimeFormat(DateFormat.FRENCH_DATE);

        // Elapsed Time
        ColumnConfig elapsedTime = GridUtils.addColumnConfig(columns,
                TaskActivityModelData.TASK_TOTAL_ACTIVITY_DURATION_FIELD, "Elapsed", 55);
        elapsedTime.setRenderer(new DurationRenderer());

        // Estimated Time
        ColumnConfig estimatedTime = GridUtils.addColumnConfig(columns, TaskActivityModelData.TASK_DURATION_FIELD,
                "Estimated", 55);
        estimatedTime.setRenderer(new DurationRenderer());

        return new ColumnModel(columns);
    }

    /**
     * Show the dialog to fill the comment of the new done approval note concerning the given task.
     * 
     * @param task
     *            the given task.
     */
    public void openTaskComment(TaskModelData task)
    {

        taskCommentDialog.setData("task", task);
        taskCommentTextArea.setValue("");
        // focus
        taskCommentDialog.setFocusWidget(taskCommentTextArea);

        taskCommentDialog.show();
    }

}
