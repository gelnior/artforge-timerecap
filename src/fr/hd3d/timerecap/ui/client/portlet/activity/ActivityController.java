package fr.hd3d.timerecap.ui.client.portlet.activity;

import java.util.Date;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.FastLongMap;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.portlet.activity.widget.TaskSelectorDisplayer;
import fr.hd3d.timerecap.ui.client.view.renderer.DatetimeUtil;


/**
 * Handles events raised by the task view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class ActivityController extends MaskableController
{
    /** Activity portlet. */
    private final IActivityPortlet view;
    /** Model that handles activity data. */
    private final ActivityModel model;
    /** Save last edited project to adapt correctly othe fields of simple activity grid. */
    private ProjectModelData lastEditedProject;

    /**
     * Default constructor.
     * 
     * @param view
     *            The portlet view.
     * @param model
     *            The model that handles activity data.
     */
    public ActivityController(IActivityPortlet view, ActivityModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == ActivityEvents.INIT)
        {
            this.onInit();
        }
        else if (type == ActivityEvents.NEXT_CLICKED)
        {
            this.onNextClicked(event);
        }
        else if (type == ActivityEvents.PREVIOUS_CLICKED)
        {
            this.onPreviousClicked(event);
        }
        else if (type == ActivityEvents.REFRESH_ACTIVITIES)
        {
            this.onRefreshActivities();
        }
        else if (type == ActivityEvents.DAY_LOADED)
        {
            this.onDayLoaded();
        }
        else if (type == ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED)
        {
            this.onWeekTotalRetrieved(event);
        }
        else if (type == ActivityEvents.TASKS_LOADED)
        {
            this.onTasksLoaded();
        }
        else if (type == ActivityEvents.TASK_ACTIVITIES_LOADED)
        {
            this.onActivityTasksLoaded();
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITIES_LOADED)
        {
            this.onSimpleActivitiesLoaded();
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_EDITED)
        {
            this.onTaskActivityEdited(event);
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITY_EDITED)
        {
            this.onSimpleActivityEdited(event);
        }
        else if (type == ActivityEvents.SIMPLE_PROJECT_CHANGED)
        {
            this.onSimpleProjectChanged(event);
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITY_SAVED)
        {
            this.onSimpleActivitiesSaved(event);
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_SAVED)
        {
            this.onTaskActivitiesSaved();
        }
        else if (type == ActivityEvents.ADD_TASK_CLICKED)
        {
            this.onAddTaskClicked();
        }
        else if (type == ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED)
        {
            this.onSelectorTaskDoubleClicked(event);
        }
        else if (type == ActivityEvents.ALL_OR_PLANNED_CLICKED)
        {
            this.onAllOrPlannedClicked(event);
        }
        else if (type == ActivityEvents.SELECTOR_PERSON_CHANGED)
        {
            this.onChangedPerson(event);
        }
        else if (type == ActivityEvents.PERSON_CHANGE)
        {
            this.onChangePersonDate(event);
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_SELECTED)
        {
            onTaskActivitySelected(event);
        }
        else if (type == ActivityEvents.STATUS_CHANGED_WAITAPP)
        {
            onStatusChangedWaitApp(event);
        }
        else if (type == ActivityEvents.ACTIVITY_MONTH_TOTAL_RETRIEVED)
        {
            onMonthTotalRetrieved(event);
        }

        this.forwardToChild(event);
    }

    /**
     * When total activity for current week loaded, task and simple activity store are reloaded.
     */
    private void onMonthTotalRetrieved(AppEvent event)
    {
        Long total = event.getData();
        this.model.setMonthTotal(total);
        this.view.updateDayTitle();
    }

    private void onStatusChangedWaitApp(AppEvent event)
    {
        TaskModelData task = event.getData();
        if (task == null)
            return;
        this.view.openTaskComment(task);
    }

    private void onTaskActivitySelected(AppEvent event)
    {
        TaskActivityModelData activity = event.getData();
        if (activity == null)
            return;

        TaskModelData task = this.model.getTaskForActivity(activity);
        // if task == null alors la task n'est pas affecter à cette personne
        if (task == null)
        {
            task = new TaskModelData();
            task.setWorkObjectId(activity.getWorkObjectId());
            task.setWorkObjectName(activity.getWorkObjectName());
            task.setProjectName(activity.getProjectName());
            task.setWorkObjectParentsName(activity.getWorkObjectParentsName());
            task.setTaskTypeName(activity.getTaskTypeName());
            task.setTaskTypeId(activity.getTaskTypeId());
        }
    }

    /**
     * Initialize widget and data.
     */
    private void onInit()
    {
        this.model.initStoreParameters();
        this.view.initWidgets();

        this.model.setCurrentPerson(MainModel.currentUser);

        // this.handleEvent(ActivityEvents.REFRESH_ACTIVITIES);
    }

    /**
     * When activities should be refreshed, the selected day is refreshed, then activities are loaded.
     */
    private void onChangedPerson(AppEvent event)
    {
        this.model.setCurrentPerson((PersonModelData) event.getData());

        this.handleEvent(ActivityEvents.REFRESH_ACTIVITIES);
    }

    /**
     * 
     */
    private void onChangePersonDate(AppEvent event)
    {
        PersonModelData person = (PersonModelData) event.getData("person");
        Date date = (Date) event.getData("date");

        this.model.setCurrentPerson(person);
        PersonDayModelData day = new PersonDayModelData();
        day.setDate(date);
        day.setPersonId(this.model.getCurrentPerson().getId());
        this.model.setCurrentDay(day);

        day.refreshOrCreate(ActivityEvents.DAY_LOADED);

        // this.view.changePerson(person);
        DateWrapper today = new DateWrapper(DatetimeUtil.today());
        DateWrapper currentDay = new DateWrapper(date);
        if (currentDay.before(today))
            this.view.enableNextButton();
        else
            this.view.disableNextButton();
    }

    /**
     * When previous button is clicked, the current day is set to the day before. All the data are refreshed depending
     * on the newly set day. Moreover next button is enabled if it was disabled.
     * 
     * @param event
     *            The previous clicked event.
     */
    private void onPreviousClicked(AppEvent event)
    {
        int availableDays = 300;

        PersonDayModelData day = new PersonDayModelData();
        DateWrapper wrapper = new DateWrapper(this.model.getCurrentDay().getDate());
        day.setDate(wrapper.addDays(-1).asDate());
        day.setPersonId(this.model.getCurrentPerson().getId());
        this.model.setCurrentDay(day);
        this.view.enableNextButton();

        DateWrapper today = new DateWrapper(DatetimeUtil.today());
        if (wrapper.addDays(-1).before(today.addDays(-1 * (availableDays - 2))))
            this.view.disablePreviousButton();

        day.refreshOrCreate(ActivityEvents.DAY_LOADED);
        event = new AppEvent(TimeRecapEvents.CALENDER_SELECTION_CHANGED);
        event.setData("date", wrapper.addDays(-1).asDate());
        EventDispatcher.forwardEvent(event);
    }

    /**
     * When next button is clicked, the current day is set to the day after. All the data are refreshed depending on the
     * newly set day. Moreover next button is disabled if newly set day is equal at today.
     * 
     * @param event
     *            The next clicked event.
     */
    private void onNextClicked(AppEvent event)
    {
        if (!this.model.getCurrentDay().getDate().equals(DatetimeUtil.today()))
        {
            PersonDayModelData day = new PersonDayModelData();
            DateWrapper wrapper = new DateWrapper(this.model.getCurrentDay().getDate());
            day.setDate(wrapper.addDays(1).asDate());
            day.setPersonId(this.model.getCurrentPerson().getId());
            this.model.setCurrentDay(day);
            this.view.enablePreviousButton();

            if (this.model.getCurrentDay().getDate().equals(DatetimeUtil.today()))
            {
                this.view.disableNextButton();
            }

            day.refreshOrCreate(ActivityEvents.DAY_LOADED);
            event = new AppEvent(TimeRecapEvents.CALENDER_SELECTION_CHANGED);
            event.setData("date", wrapper.addDays(+1).asDate());
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * When activities should be refreshed, the selected day is refreshed, then activities are loaded.
     */
    private void onRefreshActivities()
    {
        PersonDayModelData day = new PersonDayModelData();
        day.setDate(DatetimeUtil.today());
        day.setPersonId(this.model.getCurrentPerson().getId());
        this.model.setCurrentDay(day);

        day.refreshOrCreate(ActivityEvents.DAY_LOADED);
    }

    /**
     * When selected day is loaded, task store is refreshed.
     */
    private void onDayLoaded()
    {
        this.model.updateDayId();

        this.model.getTaskStore().reload();
        this.model.getSimpleActivityStore().reload();
        this.reloadWeektotal();
        this.reloadMonthTotal();
    }

    /**
     * Get from services total number of hours worked from the beginning of current week to current day.
     */
    private void reloadMonthTotal()
    {
        Date currentDate = this.model.getCurrentDay().getDate();
        DateWrapper endDate = new DateWrapper(currentDate);
        endDate = endDate.clearTime();

        DateWrapper startDate = new DateWrapper(endDate.getFirstDayOfMonth().asDate());

        this.model.reloadActivityMonthTotal(startDate.asDate(), endDate.asDate());
    }

    /**
     * Get from services total number of hours worked from the beginning of current week to current day.
     */
    private void reloadWeektotal()
    {
        Date currentDate = this.model.getCurrentDay().getDate();
        DateWrapper endDate = new DateWrapper(currentDate);
        endDate = endDate.clearTime();

        DateWrapper startDate = new DateWrapper(endDate.asDate());
        int nbDaysToSubstract = endDate.getDayInWeek() - 1;
        if (nbDaysToSubstract == -1)
        {
            nbDaysToSubstract = 6;
        }
        startDate = endDate.addDays(-1 * nbDaysToSubstract);

        this.model.reloadActivityWeekTotal(startDate.asDate(), endDate.asDate());
    }

    /**
     * When total activity for current week loaded, task and simple activity store are reloaded.
     */
    private void onWeekTotalRetrieved(AppEvent event)
    {
        Long total = event.getData();
        this.model.setWeekTotal(total);
        this.view.updateDayTitle();
    }

    /**
     * When tasks are loaded, corresponding activities are loaded.
     */
    private void onTasksLoaded()
    {
        this.model.updateTaskIds();
        this.model.getTaskActivityStore().reload();
    }

    /**
     * When activities are loaded, new activities are generated for task that don't have corresponding activities yet.
     */
    private void onActivityTasksLoaded()
    {
        FastLongMap<TaskActivityModelData> activityExistMap = new FastLongMap<TaskActivityModelData>();

        if (this.model.isFilterOn())
        {
            this.model.removeTodayFilter();
            this.model.setFilterOn(true);
        }
        for (TaskActivityModelData activity : this.model.getTaskActivityStore().getModels())
        {
            activityExistMap.put(activity.getTaskId(), activity);
        }

        for (TaskModelData task : this.model.getTaskStore().getModels())
        {
            if (activityExistMap.get(task.getId()) == null)
            {
                this.addTaskActivityToTaskActivityGrid(task);
            }
            else
            {
                this.updateTaskActivityInGrid(activityExistMap.get(task.getId()), task);
            }
        }

        if (this.model.isFilterOn())
            this.model.addTodayFilter();
        this.view.updateDayTitle();
    }

    /**
     * Add a task activity to the current task activity grid based on task given in parameter.Add to the last position.
     * 
     * @param task
     *            The task on which new activity is built.
     * */
    private TaskActivityModelData addTaskActivityToTaskActivityGrid(TaskModelData task)
    {
        return addTaskActivityToTaskActivityGrid(task, this.model.getTaskActivityStore().getCount());
    }

    /**
     * Add a task activity to the current task activity grid based on task given in parameter to given position.
     * 
     * @param task
     *            The task on which new activity is built.
     * @param position
     *            The position to add the given task.
     */
    private TaskActivityModelData addTaskActivityToTaskActivityGrid(TaskModelData task, int position)
    {

        TaskActivityModelData activity = new TaskActivityModelData();
        activity.setDayId(this.model.getCurrentDay().getId());
        activity.setTaskFields(task);
        activity.setFilledDate(DatetimeUtil.today());
        activity.setFilledById(this.model.getCurrentPerson().getId());
        activity.setWorkerId(task.getWorkerID());
        activity.setTaskTypeId(task.getTaskTypeId());
        activity.setWorkObjectId(task.getWorkObjectId());
        activity.set(TaskActivityModelData.TASK_STATUS_FIELD, task.getStatus());
        activity.set(TaskModelData.END_DATE_FIELD, task.getEndDate());
        this.model.getTaskActivityStore().insert(activity, position);
        return activity;
    }

    private void updateTaskActivityInGrid(TaskActivityModelData activity, TaskModelData task)
    {
        activity.setTaskFields(task);
        activity.setWorkerId(task.getWorkerID());
        activity.setTaskTypeId(task.getTaskTypeId());
        activity.setWorkObjectId(task.getWorkObjectId());
        activity.set(TaskActivityModelData.TASK_STATUS_FIELD, task.getStatus());
        activity.set(TaskModelData.END_DATE_FIELD, task.getEndDate());
        this.model.getTaskActivityStore().update(activity);
        this.model.getTaskActivityStore().commitChanges();
    }

    /**
     * When a task activity is edited. It is automatically saved. If duration is equal to zero and activity is not
     * linked to a task assigned to current worker, activity is deleted instead of saved.
     * 
     * @param event
     *            The grid cell edit event.
     */
    private void onTaskActivityEdited(AppEvent event)
    {
        TaskActivityModelData activity = event.getData();
        if (activity == null)
            return;
        Long duration = activity.getDuration();
        if (duration != null)
        {
            if (duration == 0)
            {
                boolean isAssigned = null == this.model.getTaskStore().findModel(TaskModelData.ID_FIELD,
                        activity.getTaskId());

                if (isAssigned || activity.getWorkerId() == null)
                {
                    activity.delete(ActivityEvents.TASK_ACTIVITY_SAVED);
                    this.model.getTaskActivityStore().remove(activity);
                }
                else
                {
                    activity.save(ActivityEvents.TASK_ACTIVITY_SAVED);
                }
            }
            else
            {
                activity.save(ActivityEvents.TASK_ACTIVITY_SAVED);
            }
        }
        this.updateStatusTaskForActivity(activity);
    }

    /**
     * When a task activity is saved, dirty markers are removed and title is updated (for number of hours rendering).
     */
    private void onTaskActivitiesSaved()
    {
        this.view.removeTaskDirty();
        this.view.updateDayTitle();
        this.reloadWeektotal();
        this.reloadMonthTotal();
        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_CHANGED, this.model.getCurrentDay());
    }

    /**
     * When activity is edited, if lastEditedProject is registered, all informations linked to project are updated. If
     * all required fields are set, data are saved.
     * 
     * @param event
     *            The simple activity edited event.
     */
    private void onSimpleActivityEdited(AppEvent event)
    {
        SimpleActivityModelData activity = event.getData();

        if (lastEditedProject != null)
        {
            activity.setProject(lastEditedProject);
            this.model.getSimpleActivityStore().update(activity);

            lastEditedProject = null;
        }

        if (activity.getProjectId() != null && activity.getDuration() != null && activity.getDuration() > 0)
        {
            if (activity.getId() == null)
            {
                this.model.addEmptySimpleActivity();
            }

            activity.save(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        }
        else if (activity.getId() != null && activity.getDuration() == 0)
        {
            activity.delete(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        }
    }

    /**
     * When simple activities are loaded, an empty line is added to let user add easily simple activities.
     */
    private void onSimpleActivitiesLoaded()
    {
        this.model.addEmptySimpleActivity();
        this.view.updateDayTitle();
    }

    /**
     * When a project is changed on a simple activity. The set value is registered, to be se after on the corresponding
     * model data.
     * 
     * @param event
     *            Project changed event.
     */
    private void onSimpleProjectChanged(AppEvent event)
    {
        lastEditedProject = event.getData();
    }

    /**
     * When simple activity is saved, hour number is updated and dirty markers are removed.
     */
    private void onSimpleActivitiesSaved(AppEvent event)
    {
        this.view.removeSimpleDirty();
        this.view.updateDayTitle();
        this.reloadWeektotal();
        this.reloadMonthTotal();

        SimpleActivityModelData activity = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        if (activity != null && activity.getDuration() == 0L)
            this.model.getSimpleActivityStore().remove(activity);

        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_CHANGED, this.model.getCurrentDay());
    }

    /**
     * When "Add task" item is clicked, the task selector is displayed to let user select another on which save time.
     */
    private void onAddTaskClicked()
    {
        this.view.displayTaskSelector();
    }

    /**
     * When task selector grid is double clicked the selected task is added to the current day task list (if not
     * already). User is now allowed to add time on this task.
     * 
     * @param event
     *            The selector task double clicked event.
     */
    private void onSelectorTaskDoubleClicked(AppEvent event)
    {
        TaskModelData task = event.getData();
        TaskSelectorDisplayer.hideDialog();

        FastMap<Boolean> activityExistMap = new FastMap<Boolean>();
        for (TaskActivityModelData activity : this.model.getTaskActivityStore().getModels())
        {
            activityExistMap.put(activity.getTaskId().toString(), true);
        }
        if (activityExistMap.get(task.getId().toString()) == null)
        {
            this.addTaskActivityToTaskActivityGrid(task);
        }
    }

    /**
     * When current day toggle button is clicked a filter is applied to task activity store to display only tasks
     * planned for current day. If toggle button is off, the filter is removed.
     */
    private void onAllOrPlannedClicked(AppEvent event)
    {
        Boolean filter = event.getData();
        if (filter)
        {
            this.model.addTodayFilter();
        }
        else
        {
            this.model.removeTodayFilter();
        }
    }

    /**
     * Save the status of the activity if it changed. It forward the event Status_changed_waitapp, if the status is
     * wait_4_approval.
     * 
     * @param activity
     *            activity whose the status changed.
     */
    public void updateStatusTaskForActivity(TaskActivityModelData activity)
    {

        TaskModelData task = this.model.getTaskMapById().get(activity.getTaskId().toString());
        if (task == null)
            return;

        Boolean right = task.getUserCanUpdate();
        if (!right)
        {
            return;
        }

        if (!task.getStatus().equals(activity.getTaskStatus()))
        {
            task.setStatus(activity.getTaskStatus());
            if (task.getStatus().equals(ETaskStatus.WAIT_APP.toString()))
            {
                EventDispatcher.forwardEvent(ActivityEvents.STATUS_CHANGED_WAITAPP, task);
            }
            else
            {

                task.save();
            }
        }
        // EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_SAVED);

    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(ActivityEvents.INIT);

        this.registerEventTypes(ActivityEvents.DAY_LOADED);
        this.registerEventTypes(ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED);
        this.registerEventTypes(ActivityEvents.REFRESH_ACTIVITIES);
        this.registerEventTypes(ActivityEvents.TASKS_LOADED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITIES_LOADED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITIES_LOADED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_EDITED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITY_EDITED);
        this.registerEventTypes(ActivityEvents.SIMPLE_PROJECT_CHANGED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_SAVED);
        this.registerEventTypes(ActivityEvents.NEXT_CLICKED);
        this.registerEventTypes(ActivityEvents.PREVIOUS_CLICKED);
        this.registerEventTypes(ActivityEvents.ADD_TASK_CLICKED);
        this.registerEventTypes(ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED);
        this.registerEventTypes(ActivityEvents.ALL_OR_PLANNED_CLICKED);
        this.registerEventTypes(ActivityEvents.SELECTOR_PERSON_CHANGED);
        this.registerEventTypes(ActivityEvents.PERSON_CHANGE);

        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_SELECTED);
        this.registerEventTypes(ActivityEvents.STATUS_CHANGED_WAITAPP);
        this.registerEventTypes(ActivityEvents.ACTIVITY_MONTH_TOTAL_RETRIEVED);

    }
}
