package fr.hd3d.timerecap.ui.client.portlet.activity;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by activity portlet.
 * 
 * @author HD3D
 */
public class ActivityEvents
{
    public static final EventType INIT = new EventType();
    public static final EventType TASKS_LOADED = new EventType();
    public static final EventType TASK_ACTIVITIES_LOADED = new EventType();
    public static final EventType LOAD_DAY = new EventType();
    public static final EventType DAY_LOADED = new EventType();
    public static final EventType REFRESH_ACTIVITIES = new EventType();
    public static final EventType TASK_ACTIVITY_EDITED = new EventType();
    public static final EventType SIMPLE_ACTIVITY_EDITED = new EventType();
    public static final EventType SIMPLE_PROJECT_CHANGED = new EventType();
    public static final EventType SIMPLE_ACTIVITIES_LOADED = new EventType();
    public static final EventType PREVIOUS_CLICKED = new EventType();
    public static final EventType NEXT_CLICKED = new EventType();
    public static final EventType SIMPLE_ACTIVITY_SAVED = new EventType();
    public static final EventType TASK_ACTIVITY_SAVED = new EventType();
    public static final EventType ADD_TASK_CLICKED = new EventType();
    public static final EventType SELECTOR_PROJECT_CHANGED = new EventType();
    public static final EventType SELECTOR_TASK_TYPE_CHANGED = new EventType();
    public static final EventType SELECTOR_TASK_DOUBLE_CLICKED = new EventType();
    public static final EventType SET_TOTAL_ACTIVITY = new EventType();
    public static final EventType ALL_OR_PLANNED_CLICKED = new EventType();
    public static final EventType SELECTOR_PERSON_CHANGED = new EventType();
    public static final EventType PERSON_CHANGE = new EventType();
    
    public static final EventType ACTIVITY_WEEK_TOTAL_RETRIEVED = new EventType();
    public static final EventType ACTIVITY_MONTH_TOTAL_RETRIEVED = new EventType();

    public static final EventType TASK_ACTIVITY_SELECTED = new EventType();
    public static final EventType STATUS_CHANGED_WAITAPP = new EventType();
    public static final EventType KEY_UP_WORKOBJECT_FILTER = new EventType();
    public static final EventType ACTIVITY_CHANGED = new EventType();

}
