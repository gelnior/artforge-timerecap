package fr.hd3d.timerecap.ui.client.portlet.activity;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;

import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * Specific filter for task activity store. It displays only activities linked to task of which id is contained in a
 * given id list.
 * 
 * @author HD3D
 */
public class ActivityStoreFilter implements StoreFilter<TaskActivityModelData>
{
    /** IDs used to filter displayed activities. */
    protected List<Long> ids = new ArrayList<Long>();

    /**
     * @return IDs used to filter displayed activities. If activity is linked to a task of which ID is not inside this
     *         list, it wont be displayed.
     */
    public List<Long> getIds()
    {
        return ids;
    }

    /**
     * If activity is linked to a task of which ID is not inside the IDs list, it is not selected.
     */
    public boolean select(Store<TaskActivityModelData> store, TaskActivityModelData parent, TaskActivityModelData item,
            String property)
    {
        return ids.contains(item.getTaskId());
    }
}
