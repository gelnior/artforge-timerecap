package fr.hd3d.timerecap.ui.client.portlet.activity.model;

import java.util.Date;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;


/**
 * Build an enf date URL parameter like "endDate=<i>date</i>".
 * 
 * @author HD3D
 */
public class EndDateParameter implements IUrlParameter
{
    /** Date to set as parameter value. */
    private final Date endDate;

    /**
     * Constructor.
     * 
     * @param EndDate
     *            Date to set as parameter value.
     */
    public EndDateParameter(Date EndDate)
    {
        this.endDate = EndDate;
    }

    /**
     * Return parameter value as string.
     */
    public String toJson()
    {
        return DateFormat.DATE_TIME.format(endDate);
    }

    /**
     * @return The string to set as parameter : EndDate=<i>EndDate</i>.
     */
    @Override
    public String toString()
    {
        return Const.ENDDATE + "=" + toJson();
    }
}
