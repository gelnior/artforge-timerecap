package fr.hd3d.timerecap.ui.client.portlet.activity.widget;

/**
 * Singleton to create task selector only on demand.
 * 
 * @author HD3D
 */
public class TaskSelectorDisplayer
{
    /** Task selector dialog allow user to select unassigned or uncreated tasks to set activity time on it. */
    public static TaskSelectorDialog dialog;

    /** Display task selector dialog (create the dialog before if it is first call). */
    public static void show()
    {
        if (dialog == null)
        {
            dialog = new TaskSelectorDialog();
        }
        dialog.show();
    }

    /** Hide task selector dialog. */
    public static void hideDialog()
    {
        dialog.hide();
    }
}
