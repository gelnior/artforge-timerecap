package fr.hd3d.timerecap.ui.client.controller;

import java.util.Date;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.user.client.ui.Hidden;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.model.RecapByPersonModel;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.timerecap.ui.client.view.IRecapByPersonView;


/**
 * Controller that handles activities.
 * 
 * @author HD3D
 */
public class RecapByPersonController extends Controller
{
    /** Model handling activity data. */
    private final RecapByPersonModel model;
    /** View displaying activities. */
    private final IRecapByPersonView view;

    private Boolean groupRefreshIsActiv = true;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling activity data.
     * @param view
     *            View displaying activities.
     */
    public RecapByPersonController(RecapByPersonModel model, IRecapByPersonView view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TimeRecapEvents.CALENDER_GRID_CREATED)
        {
            if (groupRefreshIsActiv)
            {
                this.onActivityGridCreated();
            }
            else
            {
                this.onPersonsLoaded();
            }
        }
        else if (type == TimeRecapEvents.PROJECT_LOADED)
        {
            this.projectInit();
        }
        else if (type == TimeRecapEvents.CALENDER_PERSONS_LOADED)
        {
            this.onPersonsLoaded();
        }
        else if (type == TimeRecapEvents.CALENDER_PERSONDAYS_LOADED)
        {
            this.onPersonDaysLoaded();
        }
        else if (type == TimeRecapEvents.CALENDER_FILTER_CHANGED)
        {
            this.onFilterChanged();
        }
        else if (type == TimeRecapEvents.CALENDER_GROUP_CHANGED)
        {
            this.view.setRadioGroupPerson(true);
            groupRefreshIsActiv = true;
            this.onPersonGroupChanged();
        }
        else if (type == TimeRecapEvents.SELECTOR_PERSON_CHANGED)
        {
            this.view.setRadioGroupPerson(false);
            groupRefreshIsActiv = false;
            this.onPersonChange((PersonModelData) event.getData());
        }
        else if (type == TimeRecapEvents.REFRESH_PERSON_CHANGED)
        {
            groupRefreshIsActiv = false;
            this.onRefreshPersonChange();
        }
        else if (type == TimeRecapEvents.CALENDER_REFRESH_CLICKED)
        {
            if (this.view.getCurrentGroup() != null)
            {
                groupRefreshIsActiv = true;
                this.onFilterChanged();
            }
        }
        else if (type == TimeRecapEvents.REFRESH_CLICKED)
        {
            if (this.view.isRadioGroupPerson())
            {
                EventDispatcher.forwardEvent(new AppEvent(TimeRecapEvents.CALENDER_REFRESH_CLICKED));
            }
            else
            {
                EventDispatcher.forwardEvent(new AppEvent(TimeRecapEvents.REFRESH_PERSON_CHANGED));
            }

        }
        else if (type == TimeRecapEvents.CALENDER_REFRESH)
        {
            this.onCalenderRefresh();
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized();
        }
        else if (type == TimeRecapEvents.CALENDER_SELECTION_CHANGED)
        {

            Date date = event.getData("date");
            DateWrapper wrapper = new DateWrapper(date);
            DateWrapper wrapperCurrent = new DateWrapper(this.view.getStartDate());
            if (wrapper.getMonth() != wrapperCurrent.getMonth())
            {
                event.setData("row", this.view.getCurrentRow());
                this.view.getMonthFilter().setValue(date);
            }
            this.onSelectionChanged(event);
            this.refreshSelection();
        }
        else if (type == TimeRecapEvents.CALENDER_REFRESH_CELL)
        {
            this.refreshSelection();
        }
        else if (type == ActivityEvents.ACTIVITY_CHANGED)
        {
            PersonDayModelData personDay = event.getData();
            if (personDay != null)
            {
                Date dateCalender = new DateWrapper(this.view.getStartDate()).clearTime().getFirstDayOfMonth().asDate();
                Date dateActivity = new DateWrapper(personDay.getDate()).clearTime().getFirstDayOfMonth().asDate();
                if (dateCalender.compareTo(dateActivity) != 0)
                {
                    return;
                }
            }
            this.onPersonsLoaded();
        }
        else if (type == TimeRecapEvents.ODS_EXPORT_CLICKED_BY_PERSON)
        {
            this.onExportOds(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onExportOds(AppEvent event)
    {
        String jsonToExport;
        String type = (String) event.getData();
        jsonToExport = "{" + createData(type) + "}";

        String request = "export-ods";
        Hidden hidden = new Hidden("json");
        hidden.setValue(jsonToExport);
        TimeRecapController.exportOdsFormPanel.setAction(RestRequestHandlerSingleton.getInstance().getServicesUrl()
                + request);
        TimeRecapController.exportOdsFormPanel.clear();
        TimeRecapController.exportOdsFormPanel.add(hidden);
        TimeRecapController.exportOdsFormPanel.submit();
    }

    private String createData(String type)
    {
        String dateStart = null;
        String by = "ByPerson";
        // TODO ADD THE GROUP ID

        Date dateCalender = new DateWrapper(this.view.getStartDate()).clearTime().getFirstDayOfMonth().asDate();
        dateStart = DateFormat.DATESTAMP.format(dateCalender);

        String dateEnd = DateFormat.DATESTAMP.format(this.view.getEndDate());
        return "\"timeRecap\":\"" + dateStart + "_" + dateEnd + "\", \"type\": \"" + type + by + "\", \"project-id\":"
                + null;
    }

    /**
     * When date filters changed, activity grid is rebuilt to show the selected period. The grid is hidden until all
     * data are set.
     */
    private void onFilterChanged()
    {
        if (this.view.getCurrentGroup() != null || this.view.getPersonComboBoxValue() != null)
        {
            this.model.clearActivities();

            this.view.maskGrid();
            this.view.rebuildGrid();
            this.view.deselectACell();
        }
    }

    /**
     * When date filters changed, activity grid is rebuilt to show the selected period. The grid is hidden until all
     * data are set.
     */
    private void onCalenderRefresh()
    {
        if (this.view.getCurrentGroup() != null || this.view.getPersonComboBoxValue() != null)
        {
            this.model.clearActivities();

            this.view.maskGrid();
            this.view.rebuildGrid();
        }
    }

    /**
     * When activity grid is created, it loads person data.
     */
    private void onActivityGridCreated()
    {
        this.model.loadPersons();
    }

    /**
     * When project loaded.
     */
    private void projectInit()
    {
        this.view.loadProjectComboBoxData();
    }

    /**
     * When persons are loaded, it loads event data.
     */
    private void onPersonsLoaded()
    {
        if (!this.model.personsIsEmpty())
        {
            Date startDate = this.view.getStartDate();
            Date endDate = this.view.getEndDate();
            ProjectModelData project = this.view.getProject();

            this.model.loadActivities(startDate, endDate, project);
        }
        else
        {
            this.view.unMaskGrid();
        }
    }

    /**
     * When absences are loaded, absence grid is updated with all data (persons, events and absences) then the grid is
     * unmasked.
     */

    private void onPersonDaysLoaded()
    {
        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        this.model.refreshActivities(startDate, endDate, this.view.withEmptyLines());
        this.view.unMaskGrid();

        this.view.selectACell();
    }

    /**
     * When group selection changes, it updates the absence store data and the person proxy path.
     */
    private void onPersonGroupChanged()
    {
        this.view.maskGrid();

        String path = ServicesPath.getPath(ResourceGroupModelData.SIMPLE_CLASS_NAME);
        path += this.view.getCurrentGroup().getId() + "/";
        path += ServicesPath.getPath(PersonModelData.SIMPLE_CLASS_NAME);

        this.model.setPersonPath(path);
        EventDispatcher.forwardEvent(new AppEvent(TimeRecapEvents.CALENDER_FILTER_CHANGED));
    }

    private void onRefreshPersonChange()
    {
        PersonModelData person = this.view.getPersonComboBoxValue();
        if (person != null)
        {
            AppEvent event = new AppEvent(TimeRecapEvents.SELECTOR_PERSON_CHANGED);
            event.setData(person);
            EventDispatcher.forwardEvent(event);
        }
    }

    private void onPersonChange(PersonModelData person)
    {
        this.view.deselectACell();
        this.model.addOnePerson(person);
        EventDispatcher.forwardEvent(new AppEvent(TimeRecapEvents.CALENDER_REFRESH));
    }

    private void onSelectionChanged(AppEvent event)
    {
        Date date = event.getData("date");
        DateWrapper wrapper = new DateWrapper(date);

        int col = wrapper.getDate();

        this.view.setCurrentCol(col);
        if (event.getData("row") != null)
        {
            int row = event.getData("row");
            this.view.setCurrentRow(row);
        }
    }

    private void refreshSelection()
    {
        this.view.selectACell();
    }

    /**
     * When permissions are initialized, widgets are updated.
     */
    private void onPermissionsInitialized()
    {
        this.view.initEditorPermissions();
    }

    /**
     * Register events supported by controller.
     */
    private void registerEvents()
    {
        this.registerEventTypes(TimeRecapEvents.CALENDER_FILTER_CHANGED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_GRID_CREATED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_GRID_CHANGED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_PERSONS_LOADED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_PERSONDAYS_LOADED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_GROUP_CHANGED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_REFRESH_CLICKED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_SELECTION_CHANGED);
        this.registerEventTypes(TimeRecapEvents.CALENDER_REFRESH_CELL);
        this.registerEventTypes(TimeRecapEvents.CALENDER_REFRESH);
        this.registerEventTypes(TimeRecapEvents.SELECTOR_PERSON_CHANGED);
        this.registerEventTypes(TimeRecapEvents.REFRESH_PERSON_CHANGED);
        this.registerEventTypes(TimeRecapEvents.REFRESH_CLICKED);
        this.registerEventTypes(ActivityEvents.ACTIVITY_CHANGED);
        this.registerEventTypes(TimeRecapEvents.PROJECT_LOADED);

        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);

        this.registerEventTypes(TimeRecapEvents.ODS_EXPORT_CLICKED_BY_PERSON);
    }

}
