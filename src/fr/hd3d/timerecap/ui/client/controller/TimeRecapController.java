package fr.hd3d.timerecap.ui.client.controller;

import java.util.Date;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.model.TimeRecapModel;
import fr.hd3d.timerecap.ui.client.view.TimeRecapView;


/**
 * Controller handles main events raised by project applications.
 * 
 * @author HD3D
 */
public class TimeRecapController extends MainController
{
    /** View displaying widgets. */
    final protected TimeRecapView view;
    /** Model handling data. */
    final protected TimeRecapModel model;

    private Constraint projectFilterTask;
    private Constraint projectFilterSimple;
    private Constraint datesFilter;
    private Constraint personFilter;

    public static FormPanel exportOdsFormPanel;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling Project infos data.
     * @param view
     *            View displaying Project Infos widgets.
     */
    public TimeRecapController(TimeRecapModel model, TimeRecapView view)
    {
        super(model, view);

        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    /**
     * Register events controller can handle.
     */
    @Override
    protected void registerEvents()
    {

        super.registerEvents();

        this.registerEventTypes(TimeRecapEvents.INIT_VIEW);
        this.registerEventTypes(TimeRecapEvents.PROJECT_LOADED);
        this.registerEventTypes(TimeRecapEvents.LOAD_ACTIVITIES);
        this.registerEventTypes(TimeRecapEvents.ACTIVITIES_LOADED);
        this.registerEventTypes(TimeRecapEvents.ACTIVITY_EDITED);
        this.registerEventTypes(TimeRecapEvents.ACTIVITY_SAVED);
        this.registerEventTypes(TimeRecapEvents.SIMPLE_ACTIVITIES_LOADED);
        this.registerEventTypes(TimeRecapEvents.SIMPLE_ACTIVITY_EDITED);
        this.registerEventTypes(TimeRecapEvents.SIMPLE_ACTIVITY_SAVED);
        this.registerEventTypes(TimeRecapEvents.TIME_RECAP_REFRESH_CLICKED);
        this.registerEventTypes(TimeRecapEvents.TIME_RECAP_FILTER_DATE_CHANGED);
        this.registerEventTypes(TimeRecapEvents.RAW_SELECTOR_PERSON_CHANGED);
        this.registerEventTypes(TimeRecapEvents.CHANGE_CHECK_PERSON);

        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        EventType type = event.getType();

        if (type == TimeRecapEvents.LOAD_ACTIVITIES)
        {
            this.loadData();
        }
        else if (type == TimeRecapEvents.INIT_VIEW)
        {
            this.onInitView();
        }
        else if (type == TimeRecapEvents.PROJECT_LOADED)
        {
            this.onProjectLoaded((ProjectModelData) event.getData());
        }
        else if (type == TimeRecapEvents.ACTIVITIES_LOADED)
        {
            // this.onInitSheet((IView) event.getData());
        }
        else if (type == TimeRecapEvents.TIME_RECAP_FILTER_DATE_CHANGED)
        {
            this.onTimeRecapChangedDateFilter();
        }
        else if (type == TimeRecapEvents.RAW_SELECTOR_PERSON_CHANGED)
        {
            this.onTimeRecapChangedPersonFilter();
        }
        else if (type == TimeRecapEvents.ACTIVITY_EDITED)
        {
            this.onActivityEdited(event);
        }
        else if (type == TimeRecapEvents.ACTIVITY_SAVED)
        {
            this.onActivitiesSaved();
        }
        else if (type == TimeRecapEvents.SIMPLE_ACTIVITY_EDITED)
        {
            this.onSimpleActivityEdited(event);
        }
        else if (type == TimeRecapEvents.SIMPLE_ACTIVITY_SAVED)
        {
            this.onSimpleActivitiesSaved();
        }
        else if (type == TimeRecapEvents.CHANGE_CHECK_PERSON)
        {
            this.onChangeCheckBoxPerson();
        }
        else if (type == TimeRecapEvents.TIME_RECAP_REFRESH_CLICKED)
        {
            EventDispatcher.forwardEvent(TimeRecapEvents.LOAD_ACTIVITIES);
        }
    }

    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        super.onSettingInitialized(event);

        this.view.initWidgets();

        this.view.hideStartPanel();
        createExportOdsFormPanel();
        ExportCsvController exportCsvController = new ExportCsvController(this.model, this.view);
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(exportCsvController);
        EventDispatcher.forwardEvent(TimeRecapEvents.INIT_VIEW);

    }

    private void onInitView()
    {
        // this.view.init();
        this.changedDateFilter();
        this.view.getActivityPortlet().init();
        this.view.loadProjectComboBoxData();
    }

    private void loadData()
    {
        SortInfo sortInfo = new SortInfo();
        sortInfo.setSortField("filledBy.lastName");
        sortInfo.setSortDir(SortDir.ASC);

        this.view.getTaskActivityGridWidgetView().loadData(sortInfo);
        this.view.getSimpleActivityGridWidgetView().loadData(sortInfo);
    }

    private void onActivityEdited(AppEvent event)
    {
        TaskActivityModelData activity = event.getData();
        activity.save(TimeRecapEvents.ACTIVITY_SAVED);
    }

    private void onActivitiesSaved()
    {
        this.view.removeTaskDirty();
    }

    private void onSimpleActivityEdited(AppEvent event)
    {
        SimpleActivityModelData activity = event.getData();
        activity.save(TimeRecapEvents.SIMPLE_ACTIVITY_SAVED);
    }

    private void onSimpleActivitiesSaved()
    {
        this.view.removeSimpleDirty();
    }

    private void onProjectLoaded(ProjectModelData project)
    {

        this.model.removeConstraints();
        this.model.setProjectId(project.getId());

        if (!project.getId().toString().equals("-1"))
        {
            // if (projectFilterTask == null)
            // {
            // projectFilterTask = new Constraint(EConstraintOperator.eq, "task.project.id", null);
            // }
            // this.projectFilterTask.setLeftMember(project.getId());
            // this.model.getFilterTask().setRightMember(projectFilterTask);

            // if (this.projectFilterSimple == null)
            // {
            // this.projectFilterSimple = new Constraint(EConstraintOperator.eq, "project.id", null);
            // }
            // this.projectFilterSimple.setLeftMember(project.getId());
            // this.model.getFilterSimple().setRightMember(projectFilterSimple);

            this.model.setStorePath(project);
        }
        else
        {
            this.model.getFilterTask().setRightMember(null);
            this.model.getFilterSimple().setRightMember(null);
            this.model.setStorePath(null);
        }

        this.model.addConstraints();
        EventDispatcher.forwardEvent(TimeRecapEvents.LOAD_ACTIVITIES);
    }

    private void changedDateFilter()
    {
        // Changer les filtres des Dates
        this.model.removeConstraints();

        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        if (startDate != null && endDate != null)
        {
            if (datesFilter == null)
            {
                datesFilter = new Constraint(EConstraintOperator.btw, "day.date",
                        DateFormat.TIMESTAMP_FORMAT.format(startDate), DateFormat.TIMESTAMP_FORMAT.format(endDate));
            }
            else
            {
                datesFilter.setLeftMember(DateFormat.TIMESTAMP_FORMAT.format(startDate));
                datesFilter.setRightMember(DateFormat.TIMESTAMP_FORMAT.format(endDate));
            }
            this.model.getFilterTask().setLeftMember(datesFilter);
            this.model.getFilterSimple().setLeftMember(datesFilter);
        }
        else
        {
            this.model.getFilterTask().setLeftMember(null);
            this.model.getFilterSimple().setLeftMember(null);
        }

        this.model.addConstraints();
    }

    private void changedPersonFilter()
    {

        if (this.view.getPersonComboBoxValue() != null)
        {
            this.model.removeConstraints();

            Long personId = this.view.getPersonComboBoxValue().getId();

            if (personId != null)
            {
                if (personFilter == null)
                {
                    personFilter = new Constraint(EConstraintOperator.eq, "filledBy.id", personId);
                }
                else
                {
                    personFilter.setLeftMember(personId);
                }

                this.model.getFilterGroupPersonTask().setRightMember(personFilter);
                this.model.getFilterGroupPersonSimple().setRightMember(personFilter);
            }
            else
            {
                this.model.getFilterGroupPersonTask().setRightMember(null);
                this.model.getFilterGroupPersonSimple().setRightMember(null);
            }

            this.model.addConstraints();
        }
    }

    private void onChangeCheckBoxPerson()
    {
        if (this.view.allPersons())
        {
            this.model.removeConstraints();
            this.model.getFilterGroupPersonTask().setRightMember(null);
            this.model.getFilterGroupPersonSimple().setRightMember(null);
            this.model.addConstraints();
        }
        else
        {
            changedPersonFilter();
        }

        EventDispatcher.forwardEvent(TimeRecapEvents.LOAD_ACTIVITIES);
    }

    private void onTimeRecapChangedDateFilter()
    {
        this.changedDateFilter();
        EventDispatcher.forwardEvent(TimeRecapEvents.LOAD_ACTIVITIES);
    }

    private void onTimeRecapChangedPersonFilter()
    {
        this.changedPersonFilter();
        if (this.view.allPersons())
        {
            this.view.setPersonCheckBoxValue(false);
        }
        EventDispatcher.forwardEvent(TimeRecapEvents.LOAD_ACTIVITIES);
    }

    private void createExportOdsFormPanel()
    {
        exportOdsFormPanel = new FormPanel();
        exportOdsFormPanel.setMethod(FormPanel.METHOD_POST);
        RootPanel.get().add(exportOdsFormPanel);
    }
}
