package fr.hd3d.timerecap.ui.client.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.google.gwt.user.client.ui.Hidden;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.model.TimeRecapModel;
import fr.hd3d.timerecap.ui.client.view.TimeRecapView;


public class ExportCsvController extends Controller
{

    private TimeRecapModel model;
    private TimeRecapView view;
    private String jsonToExport;

    public ExportCsvController(TimeRecapModel model, TimeRecapView view)
    {
        super();
        this.model = model;
        this.view = view;
        registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(TimeRecapEvents.ODS_EXPORT_CLICKED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (TimeRecapEvents.ODS_EXPORT_CLICKED.equals(event.getType()))
        {
            String type = (String) event.getData();
            jsonToExport = "{" + createData(type) + "}";

            String request = "export-ods";
            Hidden hidden = new Hidden("json");
            hidden.setValue(jsonToExport);
            TimeRecapController.exportOdsFormPanel.setAction(RestRequestHandlerSingleton.getInstance().getServicesUrl()
                    + request);
            TimeRecapController.exportOdsFormPanel.clear();
            TimeRecapController.exportOdsFormPanel.add(hidden);
            TimeRecapController.exportOdsFormPanel.submit();
        }
    }

    private String createData(String type)
    {
        String dateStart = null;
        String by = "";
        if (type.contains("Month"))
        {
            by = this.view.getTypeByFilter();
            // if (by.compareTo("RawData") == 0)
            // {
            // by = "";
            // }
            dateStart = DateFormat.DATESTAMP.format(this.view.getMonthFilter());
        }
        else
        {
            dateStart = DateFormat.DATESTAMP.format(this.view.getStartDate());
        }
        String dateEnd = DateFormat.DATESTAMP.format(this.view.getEndDate());

        if (this.view.getPersonComboBoxValue() != null && this.view.getPersonComboBoxValue().getId() != null)
        {
            return "\"timeRecap\":\"" + dateStart + "_" + dateEnd + "\", \"type\": \"" + type + by
                    + "\", \"project-id\":" + this.model.getProjectId() + ", \"person-id\":" + this.view.getPersonComboBoxValue().getId();
        }
        else
        {
            return "\"timeRecap\":\"" + dateStart + "_" + dateEnd + "\", \"type\": \"" + type + by
                    + "\", \"project-id\":" + this.model.getProjectId();
        }
    }

}
