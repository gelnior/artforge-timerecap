package fr.hd3d.timerecap.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;


public class TimeRecapEvents
{
    
    public static final EventType ERROR = new EventType();
    public static final EventType INIT = new EventType();
    public static final EventType INIT_SUCCESS = new EventType();
    public static final EventType INIT_CONFIG = new EventType();  
    public static final EventType INIT_VIEW = new EventType();   
    public static final EventType LOAD_TASK = new EventType();
    public static final EventType PROJECT_LOADED = new EventType();
    public static final EventType TASK_LOADED = new EventType();
    public static final EventType PREV_WEEK = new EventType();
    public static final EventType NEXT_WEEK = new EventType();
    public static final EventType LOAD_ACTIVITIES = new EventType();
    public static final EventType ACTIVITIES_LOADED = new EventType();
    public static final EventType ACTIVITY_EDITED = new EventType();
    public static final EventType ACTIVITY_SAVED = new EventType();
    public static final EventType SIMPLE_ACTIVITIES_LOADED = new EventType();
    public static final EventType SIMPLE_ACTIVITY_EDITED = new EventType();
    public static final EventType SIMPLE_ACTIVITY_SAVED = new EventType();
    public static final EventType ODS_EXPORT_CLICKED = new EventType();
    public static final EventType ODS_EXPORT_CLICKED_BY_PERSON = new EventType();
    
    public static final EventType TIME_RECAP_GRID_CREATED = new  EventType();
    public static final EventType TIME_RECAP_CREATE_TIME_RECAP_CLICKED = new  EventType();
    public static final EventType TIME_RECAP_CREATE_EVENT_CLICKED = new  EventType();
    public static final EventType TIME_RECAP_FILTER_DATE_CHANGED = new  EventType();
    public static final EventType TIME_RECAP_EVENTS_LOADED = new  EventType();
    public static final EventType TIME_RECAP_EVENT_SAVE_SUCCESS = new  EventType();
    public static final EventType TIME_RECAP_TIME_RECAPS_LOADED = new  EventType();
    public static final EventType TIME_RECAP_TIME_RECAP_SAVE_SUCCESS = new  EventType();
    public static final EventType TIME_RECAP_EVENT_EDITOR_CLICKED = new  EventType();
    public static final EventType TIME_RECAP_TIME_RECAP_EDITOR_CLICKED = new  EventType();
    public static final EventType TIME_RECAP_GROUP_CHANGED = new  EventType();
    public static final EventType TIME_RECAP_REFRESH_CLICKED = new  EventType();
    public static final EventType TIME_RECAP_TIME_RECAP_EDITOR_HIDE = new  EventType();
    
    public static final EventType CALENDER_GRID_CHANGED = new  EventType();
    public static final EventType CALENDER_PERSONS_LOADED = new EventType();
    public static final EventType CALENDER_GRID_CREATED = new EventType();
    public static final EventType CALENDER_FILTER_CHANGED = new EventType();
    public static final EventType CALENDER_LOADED = new EventType();
    public static final EventType CALENDER_GROUP_CHANGED = new EventType();
    public static final EventType CALENDER_REFRESH_CLICKED = new EventType();
  
    public static final EventType CALENDER_PERSONDAYS_LOADED = new EventType();
    public static final EventType ADMIN_GRID_SELECTION_CHANGED = new EventType();
    public static final EventType CALENDER_SELECTION_CHANGED = new EventType();
    public static final EventType CALENDER_REFRESH_CELL = new EventType();
    public static final EventType SELECTOR_PERSON_CHANGED = new EventType();
    public static final EventType REFRESH_PERSON_CHANGED = new EventType();
    public static final EventType CALENDER_REFRESH = new EventType();
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType RAW_SELECTOR_PERSON_CHANGED = new EventType();
    public static final EventType CHANGE_CHECK_PERSON = new EventType();

}
