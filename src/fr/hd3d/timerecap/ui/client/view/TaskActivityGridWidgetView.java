package fr.hd3d.timerecap.ui.client.view;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridView;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;

import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class TaskActivityGridWidgetView extends GridView
{

    private ListStore<TaskActivityModelData> store;

    private PagingToolBar pagingToolBar;

    public TaskActivityGridWidgetView(ListStore<TaskActivityModelData> store, PagingToolBar pagingToolBar)
    {
        this.store = store;
        this.pagingToolBar = pagingToolBar;
    }

    @Override
    protected void onHeaderClick(Grid<ModelData> grid, int column)
    {
        if (!headerDisabled && cm.isSortable(column))
        {
            String field = cm.getDataIndex(column);
            SortInfo sortInfo = new SortInfo(ds.getSortField(), ds.getSortDir());
            SortDir sortDir = null;

            if (sortInfo.getSortField() != null && !sortInfo.getSortField().equals(field))
            {
                sortInfo.setSortDir(SortDir.NONE);
            }

            switch (sortInfo.getSortDir())
            {
                case ASC:
                    sortDir = SortDir.DESC;
                    break;
                case NONE:
                case DESC:
                    sortDir = SortDir.ASC;
                    break;
                default:
                    sortDir = SortDir.ASC;
            }
            this.doSort(column, sortDir);
        }
    }

    @Override
    protected void doSort(int colIndex, SortDir sortDir)
    {
        String field = cm.getDataIndex(colIndex);
        SortInfo sortInfo = new SortInfo(ds.getSortField(), ds.getSortDir());

        sortInfo.setSortField(field);
        sortInfo.setSortDir(sortDir);

        if (this.pagingToolBar.getTotalPages() > 1)
        {

            this.loadData(sortInfo);
        }
        else
        {
            super.doSort(colIndex, sortInfo.getSortDir());
        }
    }

    public void loadData(SortInfo sortInfo)
    {
        PagingLoader<?> loader = (PagingLoader<?>) this.store.getLoader();
        BasePagingLoadConfig config = new BasePagingLoadConfig(0, loader.getLimit());
        if (sortInfo != null)
        {
            config.setSortInfo(sortInfo);
        }
        this.store.getLoader().load(config);
    }
}
