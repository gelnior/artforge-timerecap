package fr.hd3d.timerecap.ui.client.view;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Task status basic user is allowed to set.
 * 
 * @author HD3D
 */
public class TimeRecapTaskStatusMap extends FastMap<FieldModelData>
{
    private static final long serialVersionUID = -6051502761188822001L;

    /**
     * Default constructor.
     */
    public TimeRecapTaskStatusMap()
    {
        super();
        FieldUtils.addFieldToMap(this, 0, "Waiting for approval", ETaskStatus.WAIT_APP.toString());
        FieldUtils.addFieldToMap(this, 1, "Stand By", ETaskStatus.STAND_BY.toString());
        FieldUtils.addFieldToMap(this, 2, "Work in Progress", ETaskStatus.WORK_IN_PROGRESS.toString());
    }
}
