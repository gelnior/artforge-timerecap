package fr.hd3d.timerecap.ui.client.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoadConfig;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.ButtonGroup;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.AfterEditListener;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.ProjectAllCombobox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.basic.TaskActivityTypeRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.HoursRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.timerecap.ui.client.constant.TimeRecapConstants;
import fr.hd3d.timerecap.ui.client.controller.TimeRecapController;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.model.TimeRecapModel;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityPortlet;
import fr.hd3d.timerecap.ui.client.portlet.activity.editor.ProjectActivityCombobox;
import fr.hd3d.timerecap.ui.client.portlet.activity.editor.ProjectEditor;
import fr.hd3d.timerecap.ui.client.portlet.activity.renderer.ActivityProjectColorRenderer;
import fr.hd3d.timerecap.ui.client.view.renderer.HoursEditor;
import fr.hd3d.timerecap.ui.client.view.renderer.SimpleActivityTypeRenderer;


/**
 * This view provides all stuff to handle worker TimeRecaps.
 * 
 * @author HD3D
 */
public class TimeRecapView extends MainView
{
    /** Constant strings to display : dialog messages, button label... */
    public static TimeRecapConstants CONSTANTS = GWT.create(TimeRecapConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    private final TimeRecapModel model = new TimeRecapModel();
    /** Controller that handles main events. */
    private TimeRecapController controller;
    /**
     * Model containing person and TimeRecap row store. TimeRecap row is a model data used to display TimeRecap for one
     * person on one line.
     */
    private BorderedPanel mainPanel;
    private BorderedPanel timeRecapPanel;
    private BorderedPanel taskPanel;
    private BorderedPanel simplePanel;

    /** Start date field needed by date filter. */
    private final DateField startDateFilter = new DateField();
    /** End date field needed by date filter. */
    private final DateField endDateFilter = new DateField();

    private final DateField monthFilter = new DateField();

    private final SimpleComboBox<String> typeByFilter = new SimpleComboBox<String>();

    /** When clicked, this button refreshes the TimeRecap grid. */
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh",
            TimeRecapEvents.TIME_RECAP_REFRESH_CLICKED);

    /** When clicked, this button refreshes the TimeRecap grid. */
    private final ToolBarButton odsButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(),
            COMMON_CONSTANTS.ExportToOds(), null);

    /** When clicked, this button refreshes the TimeRecap grid. */
    private final ToolBarButton odsMonthButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(),
            COMMON_CONSTANTS.ExportToOds(), null);

    protected PersonComboBox personComboBox = new PersonComboBox();
    private final CheckBox allPersons = new CheckBox();

    private EditorGrid<TaskActivityModelData> gridTaskActivity;
    private EditorGrid<SimpleActivityModelData> gridSimpleActivity;

    final PagingToolBar toolBarTask = new PagingToolBar(50);
    final PagingToolBar toolBarSimple = new PagingToolBar(50);

    private TaskActivityGridWidgetView taskActivityGridWidgetView;
    private SimpleActivityGridWidgetView simpleActivityGridWidgetView;

    /** Project combo box allowing to select all projects. */
    private final ProjectAllCombobox projectComboBox = new ProjectAllCombobox();

    public static final long ONE_MIN = 60;
    public static final long ONE_HOUR = 60 * ONE_MIN;

    private ActivityPortlet activityPortlet;
    private RecapByPersonView recapByPersonView;

    public TimeRecapView()
    {
        super(CONSTANTS.TimeRecap());
    }

    /**
     * Default constructor. It set all widgets : filter : TimeRecap grid and TimeRecap/event forms. It registers
     * controller.
     */
    public void initWidgets()
    {

        this.mainPanel = new BorderedPanel();
        this.mainPanel.setHeaderVisible(false);

        timeRecapPanel = new BorderedPanel();
        timeRecapPanel.setHeaderVisible(false);

        this.taskPanel = new BorderedPanel();
        this.simplePanel = new BorderedPanel();

        timeRecapPanel.addCenter(taskPanel);
        timeRecapPanel.addSouth(simplePanel, 200);

        BorderedPanel basicPanel = new BorderedPanel();
        this.activityPortlet = new ActivityPortlet();
        this.activityPortlet.setHeaderVisible(true);
        BorderLayoutData activityPortletLayoutData = basicPanel.addEast(activityPortlet, 800);
        activityPortletLayoutData.setCollapsible(true);

        this.recapByPersonView = new RecapByPersonView();
        basicPanel.addCenter(recapByPersonView);

        TabItem timeRecapTabItem = new TabItem("Raw Time Recap");
        TabItem basicTabItem = new TabItem("Admin Timesheets");

        timeRecapTabItem.setLayout(new FitLayout());
        basicTabItem.setLayout(new FitLayout());

        timeRecapTabItem.add(timeRecapPanel);
        basicTabItem.add(basicPanel);

        TabPanel mainTabPanel = new TabPanel();
        mainTabPanel.add(timeRecapTabItem);
        mainTabPanel.add(basicTabItem);

        this.mainPanel.addCenter(mainTabPanel, 0, 0, 0, 0);

        this.initWidgetToolbar();
    }

    public ActivityPortlet getActivityPortlet()
    {
        return activityPortlet;
    }

    /**
     * Initializes controller and model.
     */
    @Override
    public void init()
    {

        controller = new TimeRecapController(model, this);

        EventDispatcher.get().addController(controller);

    }

    public void initWidgetToolbar()
    {
        this.setStyles();
        this.initToolbar();
        this.initFilter();
        this.createGridTaskActivity();
        this.createGridSimpleActivity();
        this.addToViewport(mainPanel);
    }

    /**
     * @return Start date field value.
     */
    public Date getStartDate()
    {
        return this.startDateFilter.getValue();
    }

    /**
     * @return Start date field value.
     */
    public Date getEndDate()
    {
        return this.endDateFilter.getValue();
    }

    /**
     * @return month date field value.
     */
    public Date getMonthFilter()
    {
        return this.monthFilter.getValue();
    }

    /**
     * @return type By filter field value.
     */
    public String getTypeByFilter()
    {
        return this.typeByFilter.getSelection().get(0).getValue().replace(" ", "");
    }

    public TaskActivityGridWidgetView getTaskActivityGridWidgetView()
    {
        return taskActivityGridWidgetView;
    }

    public SimpleActivityGridWidgetView getSimpleActivityGridWidgetView()
    {
        return simpleActivityGridWidgetView;
    }

    /**
     * Remove dirty markers on edited cells for task activity grid.
     */
    public void removeTaskDirty()
    {
        this.gridTaskActivity.getStore().commitChanges();
    }

    /**
     * Remove dirty markers on edited cells fQuel intérêt a été porté à ces documents depuis ? Ontor simple activity
     * grid.
     */
    public void removeSimpleDirty()
    {
        this.gridSimpleActivity.getStore().commitChanges();
    }

    public boolean allPersons()
    {
        return this.allPersons.getValue();
    }

    public PersonModelData getPersonComboBoxValue()
    {
        return this.personComboBox.getValue();
    }

    public void setPersonCheckBoxValue(boolean value)
    {
        this.allPersons.setValue(value);
    }

    /**
     * Load from services open projects data in project combo box.
     */
    public void loadProjectComboBoxData()
    {
        this.projectComboBox.setData();
    }

    /**
     * Initialize filter form with first day of the current month for start date and last day of the current month for
     * end date.
     */
    private void initFilter()
    {
        DateWrapper date = new DateWrapper().clearTime();

        DateWrapper beginDate = date.getFirstDayOfMonth();
        DateWrapper endDate = date.getLastDateOfMonth();

        this.startDateFilter.setValue(beginDate.asDate());
        this.endDateFilter.setValue(endDate.asDate());
        this.monthFilter.setValue(beginDate.asDate());

        this.startDateFilter.addListener(Events.Change, new EventBaseListener(
                TimeRecapEvents.TIME_RECAP_FILTER_DATE_CHANGED));
        this.endDateFilter.addListener(Events.Change, new EventBaseListener(
                TimeRecapEvents.TIME_RECAP_FILTER_DATE_CHANGED));
    }

    /**
     * Initialize TimeRecap tool bar with start date filter and end date filter.
     */
    private void initToolbar()
    {
        DateTimeFormat dtFormat = DateTimeFormat.getFormat("MMMM yyyy");
        monthFilter.getPropertyEditor().setFormat(dtFormat);

        this.timeRecapPanel.setToolBar();

        this.projectComboBox.setSelectionChangedEvent(TimeRecapEvents.PROJECT_LOADED);

        this.typeByFilter.add("By Person");
        this.typeByFilter.add("By TaskType");
        this.typeByFilter.add("By Category / Sequence");
        this.typeByFilter.add("By Work Object");
        this.typeByFilter.setFieldLabel("Name");

        this.typeByFilter.setWidth(145);

        this.typeByFilter.setForceSelection(true);
        this.typeByFilter.setTypeAhead(true);
        this.typeByFilter.setTriggerAction(TriggerAction.ALL);
        this.typeByFilter.setEditable(false);
        this.typeByFilter.setFireChangeEventOnSetValue(true);
        this.typeByFilter.setSimpleValue("By Person");

        AppEvent eventDetail = new AppEvent(TimeRecapEvents.ODS_EXPORT_CLICKED, "Detail");
        odsButton.addSelectionListener(new ButtonClickListener(eventDetail));

        Menu menu = new Menu();
        MenuItem detail = new MenuItem("Per Week");
        detail.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event = new AppEvent(TimeRecapEvents.ODS_EXPORT_CLICKED, "MonthWeek");
                EventDispatcher.forwardEvent(event);
            }
        });
        menu.add(detail);
        MenuItem monthRecap = new MenuItem("Per Day");
        monthRecap.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event = new AppEvent(TimeRecapEvents.ODS_EXPORT_CLICKED, "MonthDay");
                EventDispatcher.forwardEvent(event);
            }
        });

        menu.add(monthRecap);
        odsMonthButton.setMenu(menu);

        ButtonGroup group = new ButtonGroup(3);
        group.setHeading("Project");
        group.setToolTip(new ToolTipConfig("Information", "Select project for Raw time display or Montly export ODS"));

        group.add(new LabelField(""));
        group.add(projectComboBox);
        group.add(new LabelField(""));
        this.timeRecapPanel.addToToolBar(group);

        group = new ButtonGroup(11);
        group.setHeading("Raw time - Period for display");
        group.setToolTip(new ToolTipConfig("Information", "To display the time spent - Raw Data"));
        personComboBox.setEmptyText("Search a person...");
        personComboBox.setForceSelection(true);
        personComboBox.setWidth(140);

        personComboBox.setSelectionChangedEvent(TimeRecapEvents.RAW_SELECTOR_PERSON_CHANGED);

        // ................=> faire un explorateur ?

        group.add(new LabelField(""));
        group.add(new Label("All : "));
        this.allPersons.setValue(true);
        this.allPersons.setToolTip("All persons");

        this.allPersons.addListener(Events.Change, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(TimeRecapEvents.CHANGE_CHECK_PERSON);
            }
        });

        group.add(allPersons);
        group.add(new LabelField(""));
        group.add(personComboBox);
        group.add(new LabelField(CONSTANTS.From()));
        group.add(startDateFilter);
        group.add(new LabelField(CONSTANTS.To()));
        group.add(endDateFilter);
        group.add(refreshButton);
        group.add(odsButton);

        this.timeRecapPanel.addToToolBar(group);

        group = new ButtonGroup(5);

        group.setHeading("Monthly export ODS ");
        group.setToolTip(new ToolTipConfig("Information",
                "Montly export ODS by Person, TaskType, Category / Sequence, WorkObject : sum per Week or per Day"));

        group.add(new LabelField(""));
        group.add(monthFilter);
        group.add(new LabelField(""));
        group.add(this.typeByFilter);
        group.add(odsMonthButton);
        this.timeRecapPanel.addToToolBar(group);
    }

    /**
     * Set widget styles.
     */
    private void setStyles()
    {
        startDateFilter.setWidth(80);
        endDateFilter.setWidth(80);
        monthFilter.setWidth(110);
    }

    /**
     * Create TimeRecap grid and add it to the center.
     */
    private void createGridTaskActivity()
    {

        GridCellRenderer<TaskActivityModelData> dateRenderer = new GridCellRenderer<TaskActivityModelData>() {

            public Object render(TaskActivityModelData model, String property, ColumnData config, int rowIndex,
                    int colIndex, ListStore<TaskActivityModelData> store, Grid<TaskActivityModelData> grid)
            {
                Object obj = model.get(property);

                String stringValue = "";
                if (obj != null)
                {
                    if (obj instanceof Date)
                    {
                        Date date = (Date) model.get(property);
                        if (date != null)
                            stringValue = DateFormat.DATESTAMP.format(date);
                    }
                    else if (obj instanceof String)
                    {

                        Date date = DateFormat.TIMESTAMP_FORMAT.parse(obj.toString());
                        stringValue = DateFormat.DATESTAMP.format(date);
                    }
                }
                return stringValue;
            }
        };

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        // Project color column
        ColumnConfig projectColorColumn = new ColumnConfig(TaskModelData.PROJECT_COLOR_FIELD, FieldUtils.HTML_BLANK, 30);
        projectColorColumn.setRenderer(new ActivityProjectColorRenderer());
        projectColorColumn.setSortable(false);
        projectColorColumn.setResizable(false);
        configs.add(projectColorColumn);

        // Project name column
        ColumnConfig projectNameColumnConfig = new ColumnConfig(TaskActivityModelData.PROJECT_NAME_FIELD,
                COMMON_CONSTANTS.Project(), 150);
        projectNameColumnConfig.setSortable(false);
        configs.add(projectNameColumnConfig);

        ColumnConfig projectTypeNameColumnConfig = new ColumnConfig(TaskActivityModelData.PROJECT_TYPE_NAME_FIELD,
                COMMON_CONSTANTS.ProjectType(), 100);
        projectTypeNameColumnConfig.setSortable(false);
        configs.add(projectTypeNameColumnConfig);

        // Type column
        ColumnConfig taskTypeNameColumnConfig = new ColumnConfig(TaskActivityModelData.TASK_TYPE_NAME_FIELD,
                COMMON_CONSTANTS.Type(), 120);
        taskTypeNameColumnConfig.setRenderer(new TaskActivityTypeRenderer());
        taskTypeNameColumnConfig.setSortable(false);
        configs.add(taskTypeNameColumnConfig);

        // Work object name column
        ColumnConfig parentColumnConfig = new ColumnConfig(TaskActivityModelData.WORK_OBJECT_PARENTS_NAME_FIELD,
                "Parent", 120);
        parentColumnConfig.setSortable(false);
        configs.add(parentColumnConfig);

        ColumnConfig workObjectNameColumnConfig = new ColumnConfig(TaskActivityModelData.WORK_OBJECT_NAME_FIELD,
                COMMON_CONSTANTS.WorkObject(), 150);
        workObjectNameColumnConfig.setSortable(false);
        configs.add(workObjectNameColumnConfig);

        // ColumnConfig taskNameColumnConfig = new ColumnConfig("taskName", "Task Name", 200);
        // taskNameColumnConfig.setSortable(false);
        // configs.add(taskNameColumnConfig);

        ColumnConfig durationColumnConfig = new ColumnConfig("duration", "Duration", 60);
        durationColumnConfig.setRenderer(new HoursRenderer<ModelData>());
        configs.add(durationColumnConfig);
        durationColumnConfig.setEditor(new FieldComboBoxEditor(new HoursEditor(12)));

        ColumnConfig workerNameColumnConfig = new ColumnConfig("filledByName", "Worker", 200);
        workerNameColumnConfig.setSortable(false);
        configs.add(workerNameColumnConfig);

        ColumnConfig dayDateColumnConfig = new ColumnConfig("dayDate", "Date", 150);
        dayDateColumnConfig.setRenderer(dateRenderer);
        configs.add(dayDateColumnConfig);

        ColumnConfig taskCommentColumnConfig = new ColumnConfig("comment", "Comment", 600);
        taskCommentColumnConfig.setSortable(false);
        configs.add(taskCommentColumnConfig);

        final ColumnModel cm = new ColumnModel(configs);

        PagingLoader<?> loader = (PagingLoader<?>) this.model.getTaskActivitiesStore().getLoader();
        PagingLoadConfig loadConfig = new BasePagingLoadConfig();
        loadConfig.setSortField("taskName");
        toolBarTask.bind(loader);

        this.gridTaskActivity = new EditorGrid<TaskActivityModelData>(this.model.getTaskActivitiesStore(), cm);
        this.taskActivityGridWidgetView = new TaskActivityGridWidgetView(this.model.getTaskActivitiesStore(),
                toolBarTask);
        this.gridTaskActivity.addListener(Events.AfterEdit, new AfterEditListener<TaskActivityModelData>(
                TimeRecapEvents.ACTIVITY_EDITED));

        this.gridTaskActivity.setView(this.taskActivityGridWidgetView);

        this.gridTaskActivity.setBorders(true);
        this.gridTaskActivity.setHideHeaders(false);

        this.taskPanel.setBottomComponent(toolBarTask);
        this.taskPanel.addCenter(gridTaskActivity, 0, 0, 0, 0);
        this.taskPanel.setToolTip("Time spent on planned tasks");
        this.taskPanel.setHeading("Time spent on planned tasks");
        this.taskPanel.setHeaderVisible(true);
        this.taskPanel.getHeader().setBorders(false);
        this.taskPanel.setBorders(true);
    }

    /**
     * Create TimeRecap grid and add it to the center.
     */
    private void createGridSimpleActivity()
    {

        GridCellRenderer<SimpleActivityModelData> dateRenderer = new GridCellRenderer<SimpleActivityModelData>() {

            public Object render(SimpleActivityModelData model, String property, ColumnData config, int rowIndex,
                    int colIndex, ListStore<SimpleActivityModelData> store, Grid<SimpleActivityModelData> grid)
            {
                Object obj = model.get(property);

                String stringValue = "";
                if (obj != null)
                {
                    if (obj instanceof Date)
                    {
                        Date date = (Date) model.get(property);
                        if (date != null)
                            stringValue = DateFormat.DATESTAMP.format(date);
                    }
                    else if (obj instanceof String)
                    {

                        Date date = DateFormat.TIMESTAMP_FORMAT.parse(obj.toString());
                        stringValue = DateFormat.DATESTAMP.format(date);
                    }
                }
                return stringValue;
            }
        };

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        // Project color column
        ColumnConfig projectColorColumn = new ColumnConfig(TaskModelData.PROJECT_COLOR_FIELD, FieldUtils.HTML_BLANK, 30);
        projectColorColumn.setRenderer(new ActivityProjectColorRenderer());
        projectColorColumn.setSortable(false);
        projectColorColumn.setResizable(false);
        configs.add(projectColorColumn);

        // Project name column
        ColumnConfig projectNameColumnConfig = new ColumnConfig(SimpleActivityModelData.PROJECT_NAME_FIELD,
                COMMON_CONSTANTS.Project(), 150);
        projectNameColumnConfig.setSortable(false);
        projectNameColumnConfig.setEditor(new ProjectEditor(new ProjectActivityCombobox()));
        configs.add(projectNameColumnConfig);

        ColumnConfig projectTypeNameColumnConfig = new ColumnConfig("projectTypeName", "Project Type", 100);
        projectTypeNameColumnConfig.setSortable(false);
        configs.add(projectTypeNameColumnConfig);

        ColumnConfig taskTypeNameColumnConfig = new ColumnConfig(SimpleActivityModelData.TYPE_FIELD,
                COMMON_CONSTANTS.Type(), 120);
        taskTypeNameColumnConfig.setSortable(false);
        taskTypeNameColumnConfig.setRenderer(new SimpleActivityTypeRenderer());
        configs.add(taskTypeNameColumnConfig);

        ColumnConfig taskNameColumnConfig = new ColumnConfig("comment", "Comment", 470);
        taskNameColumnConfig.setSortable(false);
        configs.add(taskNameColumnConfig);

        ColumnConfig durationColumnConfig = new ColumnConfig("duration", "Duration", 60);
        durationColumnConfig.setRenderer(new HoursRenderer<ModelData>());
        configs.add(durationColumnConfig);
        durationColumnConfig.setEditor(new FieldComboBoxEditor(new HoursEditor(12)));

        ColumnConfig workerNameColumnConfig = new ColumnConfig("filledByName", "Worker", 200);
        workerNameColumnConfig.setSortable(false);
        configs.add(workerNameColumnConfig);

        ColumnConfig dayDateColumnConfig = new ColumnConfig("dayDate", "Date", 150);
        dayDateColumnConfig.setRenderer(dateRenderer);
        configs.add(dayDateColumnConfig);

        final ColumnModel cm = new ColumnModel(configs);

        PagingLoader<?> loader = (PagingLoader<?>) this.model.getSimpleActivitiesListStore().getLoader();
        PagingLoadConfig loadConfig = new BasePagingLoadConfig();
        loadConfig.setSortField(SimpleActivityModelData.PROJECT_NAME_FIELD);
        toolBarSimple.bind(loader);

        this.gridSimpleActivity = new EditorGrid<SimpleActivityModelData>(this.model.getSimpleActivitiesListStore(), cm);
        this.simpleActivityGridWidgetView = new SimpleActivityGridWidgetView(this.model.getSimpleActivitiesListStore(),
                toolBarSimple);
        this.gridSimpleActivity.addListener(Events.AfterEdit, new AfterEditListener<SimpleActivityModelData>(
                TimeRecapEvents.SIMPLE_ACTIVITY_EDITED));
        this.gridSimpleActivity.setView(this.simpleActivityGridWidgetView);

        this.gridSimpleActivity.setBorders(true);

        this.simplePanel.setBottomComponent(toolBarSimple);
        this.simplePanel.addCenter(gridSimpleActivity, 0, 0, 0, 0);
        this.simplePanel.setToolTip("Unplanned tasks");
        this.simplePanel.setHeading("Unplanned tasks");
        this.simplePanel.setHeaderVisible(true);
        this.simplePanel.getHeader().setBorders(false);
        this.simplePanel.setBorders(true);
    }
}
