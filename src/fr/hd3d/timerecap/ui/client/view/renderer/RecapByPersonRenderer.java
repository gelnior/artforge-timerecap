package fr.hd3d.timerecap.ui.client.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.timerecap.ui.client.modeldata.RecapByPersonRowModelData;


/**
 * Absence Renderer display colors for absence value : Red for 'A' (absence), Gray for 'W' (week-end), Green for 'E'
 * (event) and no color for 'P' (presence);
 * 
 * @author HD3D
 */
public class RecapByPersonRenderer implements GridCellRenderer<RecapByPersonRowModelData>
{
    /** CSS Red Color. */
    public static final String RED = "red";
    /** CSS Gray Color. */
    public static final String GRAY = "#DDD";
    /** CSS Green Color. */
    public static final String GREEN = "#CFC";
    /** CSS Green Color. */
    public static final String NOCOLOR = "";

    public Object render(RecapByPersonRowModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<RecapByPersonRowModelData> store, Grid<RecapByPersonRowModelData> grid)
    {
        String val = model.get(property);
        long duration = 0L;

        if (val == RecapByPersonRowModelData.PRESENCE)
        {
            return getDivValue(NOCOLOR, duration);
        }
        else if (val == RecapByPersonRowModelData.WEEKEND)
        {
            return getDivValue(GRAY, duration);
        }
        else
        {
            duration = Long.parseLong(val);
            return getDivValue(NOCOLOR, duration);
        }
    }

    private String getDivValue(String color, long duration)
    {
        if (duration == 0){
            return "<div style=\"background-color: " + color + "; witdth: 100%; height: 100%;text-align:center;\">&nbsp;</div>";
        } 
        
        return  "<div style='width:100%; height:100%; text-align:center; background-color:" + color + ";'>"
        + (float) duration/3600 + "</div>";
        
    }
}
