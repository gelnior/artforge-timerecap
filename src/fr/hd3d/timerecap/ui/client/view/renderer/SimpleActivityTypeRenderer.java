package fr.hd3d.timerecap.ui.client.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;


public class SimpleActivityTypeRenderer implements GridCellRenderer<SimpleActivityModelData>
{

    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public Object render(SimpleActivityModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<SimpleActivityModelData> store, Grid<SimpleActivityModelData> grid)
    {
        String type = model.get(property);

        return getTypeRendered(type);
    }

    private String getTypeRendered(String type)
    {
        String cellCode = "";
        if (!Util.isEmptyString(type))
        {
            cellCode = type;
            if (type.compareTo(ESimpleActivityType.MEETING.toString()) == 0)
            {
                cellCode = COMMON_CONSTANTS.Meeting();

            }
            else if (type.compareTo(ESimpleActivityType.TECHNICAL_INCIDENT.toString()) == 0)
            {
                cellCode = COMMON_CONSTANTS.TechnicalIncident();

            }
            else if (type.compareTo(ESimpleActivityType.OTHER.toString()) == 0)
            {
                cellCode = COMMON_CONSTANTS.Other();
            }
        }
        else
        {
            cellCode = "";
        }
        return cellCode;
    }

}
