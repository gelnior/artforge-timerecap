package fr.hd3d.timerecap.ui.client.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.timerecap.ui.client.modeldata.RecapByPersonRowModelData;


/**
 * Absence Renderer display colors for absence value : Red for 'A' (absence), Gray for 'W' (week-end), Green for 'E'
 * (event) and no color for 'P' (presence);
 * 
 * @author HD3D
 */
public class RecapByPersonTotalRenderer implements GridCellRenderer<RecapByPersonRowModelData>
{

    public static final String NOCOLOR = "";

    public Object render(RecapByPersonRowModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<RecapByPersonRowModelData> store, Grid<RecapByPersonRowModelData> grid)
    {
        //long val = model.get(property);
        long val = model.getTotal();
       

            return getDivValue(NOCOLOR,val);
     
    }

    private String getDivValue(String color, long duration)
    {
        if (duration == 0)
        {
            return "";
        }
       
        return "<div style= 'font-weight: bold;text-align:center;'>" + (float) duration / 3600 + "</div>";
    }
}
