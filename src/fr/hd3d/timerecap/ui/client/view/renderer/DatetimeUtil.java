package fr.hd3d.timerecap.ui.client.view.renderer;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.calendar.DateFormat;


public class DatetimeUtil
{
    public static final int HOUR_SECONDS = 3600;
    public static final int QUARTER_HOUR_SECONDS = 900;
    public static final int MINUTE_SECONDS = 60;

    public static Date today()
    {
        return new DateWrapper(new Date()).clearTime().asDate();
    }

    public static String todayAsString()
    {
        return DateFormat.DATE_TIME.format(today());
    }

    public static Date tomorrow()
    {
        DateWrapper wrapper = new DateWrapper(new Date()).clearTime();
        return wrapper.addDays(1).asDate();
    }

    public static String tomorrowAsString()
    {
        return DateFormat.DATE_TIME.format(tomorrow());
    }

    public static Long getDaySeconds(Date date)
    {
        DateWrapper wrapper = new DateWrapper(date);
        Long seconds = 0L;
        seconds += wrapper.getHours() * HOUR_SECONDS;
        seconds += wrapper.getMinutes() * MINUTE_SECONDS;
        return seconds;
    }

    public static Boolean isTimeBefore(Date date, Date date2)
    {
        return getDaySeconds(date) <= getDaySeconds(date2);
    }
    
    public static String getDayFromDayInWeek(int dayInWeek)
    {
        switch (dayInWeek)
        {
            case 0:
                return "sunday";
            case 1:
                return "monday";
            case 2:
                return "tuesday";
            case 3:
                return "wednesday";
            case 4:
                return "thursday";
            case 5:
                return "friday";
            case 6:
                return "saturday";
            default:
                ;
        }
        return null;
    }
    
    public static String getMonthString(int month)
    {
        switch (month)
        {
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
            default:
                ;
        }
        return null;
    }
}
