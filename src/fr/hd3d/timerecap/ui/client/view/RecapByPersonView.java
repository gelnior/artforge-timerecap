package fr.hd3d.timerecap.ui.client.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.RadioGroup;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.client.constants.DateTimeConstants;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.AutoGroupComboBox;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.ProjectAllCombobox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;
import fr.hd3d.common.ui.client.widget.grid.SimpleLineCellSelectionModel;
import fr.hd3d.timerecap.ui.client.constant.TimeRecapConstants;
import fr.hd3d.timerecap.ui.client.controller.RecapByPersonController;
import fr.hd3d.timerecap.ui.client.event.TimeRecapEvents;
import fr.hd3d.timerecap.ui.client.model.RecapByPersonModel;
import fr.hd3d.timerecap.ui.client.modeldata.RecapByPersonRowModelData;
import fr.hd3d.timerecap.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.timerecap.ui.client.view.renderer.DatetimeUtil;
import fr.hd3d.timerecap.ui.client.view.renderer.RecapByPersonRenderer;
import fr.hd3d.timerecap.ui.client.view.renderer.RecapByPersonTotalRenderer;


public class RecapByPersonView extends BorderedPanel implements IRecapByPersonView
{
    /** Constant strings to display : dialog messages, button label... */
    public static TimeRecapConstants CONSTANTS = GWT.create(TimeRecapConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Model containing person and recap row store. Recap row is a model data used to display recap for one person on
     * one line.
     */
    private final RecapByPersonModel model = new RecapByPersonModel();
    /** Controller that handles Time Recap events. */
    private final RecapByPersonController controller = new RecapByPersonController(model, this);
    /** Start date field needed by date filter. */
    private final DateField startDateFilter = new DateField();
    /** End date field needed by date filter. */
    private final DateField endDateFilter = new DateField();

    /** When clicked, this button refreshes the Time Recap grid. */
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh",
            TimeRecapEvents.REFRESH_CLICKED);

    /** When clicked, this button empty line the Time Recap grid. */
    private final CheckBox withEmpty = new CheckBox();

    private final Radio radioGroupPerson = new Radio();

    private final Radio radioPerson = new Radio();

    /** Grid used to display TimeRecap. */
    private Grid<RecapByPersonRowModelData> grid;

    public static final String DATE_STRING = "EEE";
    public static DateTimeFormat DATE_F = DateTimeFormat.getFormat(DATE_STRING);

    /** When clicked, this button send the Calender grid in csv file. */
    private final ToolBarButton odsButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(),
            COMMON_CONSTANTS.ExportToOds(), null);

    /**
     * When you select a group in that combo box, Time Recap grid is refreshed with Time Recap data of persons from that
     * group.
     */
    private final AutoGroupComboBox groupCombo = new AutoGroupComboBox();

    protected PersonComboBox personComboBox = new PersonComboBox();

    // private final BaseDateField monthFilter = new BaseDateField(); TODO

    private final DateField monthFilter = new DateField();

    private SimpleLineCellSelectionModel<RecapByPersonRowModelData> selectionModel;

    private final ProjectAllCombobox projectComboBox = new ProjectAllCombobox();;

    int currentRow = -1;
    int currentCol = -1;

    /**
     * Default constructor. It set all widgets : filter : Time Recap grid. It registers controller.
     */
    public RecapByPersonView()
    {
        super();
        EventDispatcher.get().addController(controller);

        this.initToolbar();
        this.initFilter();
        this.createGrid();

        this.controller.handleEvent(new AppEvent(CommonEvents.PERMISSION_INITIALIZED));
        this.groupCombo.addSelectionChangedListener(new EventSelectionChangedListener<ResourceGroupModelData>(
                TimeRecapEvents.CALENDER_GROUP_CHANGED));
        this.groupCombo.setEmptyText("Select a person group");
    }

    /**
     * Rebuild grid with a new column model depending on the date filters.
     */
    public void rebuildGrid()
    {
        ColumnModel cm = this.getColumnModel(this.getStartDate(), this.getEndDate());
        this.grid.reconfigure(this.model.getActivityListStore(), cm);

        this.controller.handleEvent(new AppEvent(TimeRecapEvents.CALENDER_GRID_CREATED));
    }

    /**
     * @return Start date field value.
     */
    public Date getStartDate()
    {
        Date date = new DateWrapper(this.monthFilter.getValue()).clearTime().getFirstDayOfMonth().asDate();
        return date;
    }

    /**
     * @return Start date field value.
     */
    public Date getEndDate()
    {
        DateWrapper date = new DateWrapper(this.monthFilter.getValue()).clearTime();
        Date endDate = date.getLastDateOfMonth().asDate();

        return endDate;
    }

    public DateField getMonthFilter()
    {
        return monthFilter;
    }

    /**
     * @return project field value.
     */
    public ProjectModelData getProject()
    {
        ProjectModelData project = this.projectComboBox.getValue();
        return project;
    }

    /**
     * @return Sum Activities grid selected rows.
     */
    public List<RecapByPersonRowModelData> getSelection()
    {
        return this.grid.getSelectionModel().getSelectedItems();
    }

    /**
     * @return Selected group in group combo box (group currently displayed).
     */
    public ResourceGroupModelData getCurrentGroup()
    {
        return this.groupCombo.getValue();
    }

    /**
     * Mask grid and display a loading message.
     */
    public void maskGrid()
    {
        this.grid.mask(COMMON_CONSTANTS.Loading());
    }

    /**
     * Unmask grid and remove loading message.
     */
    public void unMaskGrid()
    {
        this.grid.unmask();
    }

    /**
     * Set Group combo enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setGroupComboEnabled(boolean enabled)
    {
        this.groupCombo.setEnabled(enabled);
    }

    /**
     * Set refresh button enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setRefreshEnabled(boolean enabled)
    {
        this.refreshButton.setEnabled(enabled);
    }

    /**
     * Ask for absence and event editor to hide/show their widgets depending on permissions.
     */
    public void initEditorPermissions()
    {}

    /**
     * @return Activity selected in model data.
     */
    public RecapByPersonRowModelData getActivityEditorSelection()
    {
        return null;
    }

    /**
     * Load from services open projects data in project combo box.
     */
    public void loadProjectComboBoxData()
    {
        this.projectComboBox.setData();
    }

    /**
     * Initialize filter form with first day of the current month for start date and last day of the current month for
     * end date.
     */
    private void initFilter()
    {
        DateWrapper date = new DateWrapper().clearTime();

        DateWrapper beginDate = date.getFirstDayOfMonth();
        DateWrapper endDate = date.getLastDateOfMonth();

        this.startDateFilter.setValue(beginDate.asDate());
        this.endDateFilter.setValue(endDate.asDate());
        this.monthFilter.setValue(beginDate.asDate());

        this.monthFilter.setEditable(false);
        this.monthFilter.setValidateOnBlur(false);
        this.monthFilter.setFireChangeEventOnSetValue(true);
        this.monthFilter.addListener(Events.Change, new EventBaseListener(TimeRecapEvents.CALENDER_FILTER_CHANGED));

    }

    public PersonModelData getPersonComboBoxValue()
    {
        return this.personComboBox.getValue();
    }

    /**
     * Initialize absence tool bar with start date filter and end date filter.
     */
    private void initToolbar()
    {
        this.setBorders(true);
        this.setToolBar();

        DateTimeFormat dtFormat = DateTimeFormat.getFormat("MMMM yyyy");
        this.monthFilter.getPropertyEditor().setFormat(dtFormat);
        this.monthFilter.setWidth(110);

        this.addToToolBar(this.monthFilter);
        this.addToToolBar(new SeparatorToolItem());

        this.addToToolBar(new Label("Empty lines : "));
        this.withEmpty.setValue(true);
        this.withEmpty.setTitle("Empty lines");
        this.addToToolBar(withEmpty);

        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(groupCombo);
        this.addToToolBar(new SeparatorToolItem());

        personComboBox.setEmptyText("Search a person...");
        personComboBox.setForceSelection(true);
        personComboBox.setWidth(140);
        personComboBox.setSelectionChangedEvent(ActivityEvents.SELECTOR_PERSON_CHANGED);
        personComboBox.setSelectionChangedEvent(TimeRecapEvents.SELECTOR_PERSON_CHANGED);
        this.addToToolBar(personComboBox);
        this.addToToolBar(new SeparatorToolItem());

        this.radioGroupPerson.setBoxLabel("Group");
        this.radioGroupPerson.setValue(true);

        this.radioPerson.setBoxLabel("Person");

        RadioGroup radioGroup = new RadioGroup();
        radioGroup.setToolTip("Use for refresh");
        radioGroup.add(this.radioGroupPerson);
        radioGroup.add(this.radioPerson);
        this.radioPerson.setEnabled(false);
        this.radioGroupPerson.setEnabled(false);

        radioGroup.addListener(Events.Change, new EventBaseListener(TimeRecapEvents.REFRESH_CLICKED));

        this.addToToolBar(radioGroup);
        this.addToToolBar(refreshButton);
        this.setRefreshEnabled(true);

        AppEvent eventDetail = new AppEvent(TimeRecapEvents.ODS_EXPORT_CLICKED_BY_PERSON, "MonthDay");
        odsButton.addSelectionListener(new ButtonClickListener(eventDetail));

        projectComboBox.setToolTip("Filter on project");
        this.addToToolBar(new SeparatorToolItem());
        this.projectComboBox.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                TimeRecapEvents.CALENDER_REFRESH));
        this.addToToolBar(projectComboBox);
    }

    /**
     * Create activity grid and add it to the center.
     */
    private void createGrid()
    {
        ColumnModel cm = this.getColumnModel(this.getStartDate(), this.getEndDate());

        this.grid = new Grid<RecapByPersonRowModelData>(this.model.getActivityListStore(), cm);
        SelectableGridView gridView = new SelectableGridView();
        this.grid.setView(gridView);
        this.selectionModel = new SimpleLineCellSelectionModel<RecapByPersonRowModelData>(gridView,
                TimeRecapEvents.ADMIN_GRID_SELECTION_CHANGED);
        this.grid.setSelectionModel(this.selectionModel);

        this.grid.setBorders(true);

        Listener<GridEvent<RecapByPersonRowModelData>> gridListener = new Listener<GridEvent<RecapByPersonRowModelData>>() {
            public void handleEvent(GridEvent<RecapByPersonRowModelData> be)
            {

                RecapByPersonRowModelData selected = be.getModel();
                if (selected != null)
                {
                    PersonModelData person = selected.getPerson();

                    int col = be.getColIndex();
                    int row = be.getRowIndex();

                    if (col != -1)
                    {
                        DateWrapper wrapper = new DateWrapper(getStartDate());
                        col = col - 2;
                        Date date = wrapper.addDays(+col).asDate();

                        if (!date.after(DatetimeUtil.today()))
                        {

                            AppEvent event = new AppEvent(ActivityEvents.PERSON_CHANGE);
                            event.setData("person", person);
                            event.setData("date", date);
                            EventDispatcher.forwardEvent(event);

                            event = new AppEvent(TimeRecapEvents.CALENDER_SELECTION_CHANGED);
                            event.setData("date", date);
                            event.setData("row", row);
                            EventDispatcher.forwardEvent(event);
                        }
                        else
                        {
                            EventDispatcher.forwardEvent(new AppEvent(TimeRecapEvents.CALENDER_REFRESH_CELL));
                        }
                    }
                }
            }
        };

        this.grid.addListener(Events.OnClick, gridListener);
        this.addCenter(grid);
    }

    public int getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(int currentRow)
    {
        this.currentRow = currentRow;
    }

    public int getCurrentCol()
    {
        return currentCol;
    }

    public void setCurrentCol(int currentCol)
    {
        this.currentCol = currentCol;
    }

    public boolean withEmptyLines()
    {
        return this.withEmpty.getValue();
    }

    public Boolean isRadioGroupPerson()
    {
        return radioGroupPerson.getValue();
    }

    public void setRadioGroupPerson(Boolean bool)
    {
        if (bool)
        {
            this.radioGroupPerson.setEnabled(true);
            this.radioGroupPerson.setValue(true);
            this.radioPerson.setValue(false);
        }
        else
        {
            this.radioPerson.setEnabled(true);
            this.radioGroupPerson.setValue(false);
            this.radioPerson.setValue(true);
        }

    }

    /**
     * 
     */
    public void selectACell()
    {
        this.selectionModel.deselectAll();
        this.selectionModel.selectRowAndCell(getCurrentRow(), getCurrentCol() + 1, false);
    }

    public void deselectACell()
    {
        this.selectionModel.deselectAll();
        this.setCurrentRow(-1);
        this.setCurrentCol(-1);
    }

    /**
     * @param begin
     *            Begin date of absence displayed.
     * @param end
     *            End date of absence displayed.
     * @return Column model containing one column for the person full name (first + last) and one column for each from
     *         <i>begin</i> date to <i>end</i> date.
     */
    private ColumnModel getColumnModel(Date begin, Date end)
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();
        DateWrapper beginDate = new DateWrapper(begin);
        DateWrapper endDate = new DateWrapper(end);

        this.addNameColumn(columnConfigs);
        this.addTotalColumn(columnConfigs);
        this.addDayColumns(columnConfigs, beginDate, endDate);
        this.addTotalColumn(columnConfigs);

        beginDate = new DateWrapper(begin);
        ColumnModel cm = new ColumnModel(columnConfigs);
        this.addHeaders(cm, beginDate, endDate);

        return cm;
    }

    /**
     * Add a column displaying person full name.
     * 
     * @param columnConfigs
     *            The column config list on which to add the name column.
     */
    private void addNameColumn(List<ColumnConfig> columnConfigs)
    {
        ColumnConfig nameCC = new ColumnConfig();
        nameCC.setDataIndex(NameModelData.NAME_FIELD);
        nameCC.setHeader(CONSTANTS.Person());
        nameCC.setWidth(140);
        columnConfigs.add(nameCC);
    }

    /**
     * Add a column displaying total
     * 
     * @param columnConfigs
     *            The column config list on which to add the name column.
     */
    private void addTotalColumn(List<ColumnConfig> columnConfigs)
    {
        ColumnConfig totalCC = new ColumnConfig();
        totalCC.setDataIndex("total");
        totalCC.setHeader("Total");
        totalCC.setWidth(45);
        totalCC.setRenderer(new RecapByPersonTotalRenderer());
        columnConfigs.add(totalCC);
    }

    /**
     * Add a day column for each date between <i>beginDate</i> and <i>endDate</i>.
     * 
     * @param columnConfigs
     *            The column config list on which to add the day columns.
     * @param beginDate
     *            Begin date for day column list.
     * @param endDate
     *            End date for day column list.
     */
    private void addDayColumns(List<ColumnConfig> columnConfigs, DateWrapper beginDate, DateWrapper endDate)
    {
        RecapByPersonRenderer recapByPersonRenderer = new RecapByPersonRenderer();

        while (beginDate.before(endDate.addDays(1)))
        {
            ColumnConfig cc = new ColumnConfig();

            String key = beginDate.getFullYear() + "-" + beginDate.getMonth() + "-" + beginDate.getDate();
            cc.setDataIndex(key);
            cc.setHeader(" " + beginDate.getDate());
            int index = beginDate.getDate();
            cc.setId(String.valueOf(index));
            cc.setWidth(30);
            cc.setResizable(false);
            cc.setSortable(false);
            cc.setRenderer(recapByPersonRenderer);
            columnConfigs.add(cc);

            beginDate = beginDate.addDays(1);
        }
    }

    /**
     * Add Headers to the grid column model for each day column.
     * 
     * @param cm
     *            The column model.
     * @param beginDate
     *            The begin date of day columns.
     * @param endDate
     *            The end date of day columns.
     */
    private void addHeaders(ColumnModel cm, DateWrapper beginDate, DateWrapper endDate)
    {
        int column = 2;
        DateWrapper iterator = new DateWrapper(beginDate.asDate());

        while (iterator.before(endDate))
        {
            int width = 0;
            if (iterator.getMonth() != endDate.getMonth())
            {
                width = iterator.getLastDateOfMonth().getDate() - iterator.getDate() + 1;
            }
            else
            {
                width = endDate.getDate() - iterator.getDate() + 1;
            }

            DateTimeConstants constants = LocaleInfo.getCurrentLocale().getDateTimeConstants();
            cm.addHeaderGroup(0, column,
                    new HeaderGroupConfig(constants.months()[iterator.getMonth()] + " " + iterator.getFullYear(), 1,
                            width));

            column += width;
            iterator = iterator.addMonths(1);
            if (iterator.getDate() != 1)
            {
                iterator = iterator.getFirstDayOfMonth();
            }
        }
    }

}
