package fr.hd3d.timerecap.ui.client.view;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.widget.form.DateField;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.timerecap.ui.client.modeldata.RecapByPersonRowModelData;


public interface IRecapByPersonView
{

    Date getEndDate();

    Date getStartDate();
    
    ProjectModelData getProject();

    void rebuildGrid();

    List<RecapByPersonRowModelData> getSelection();

    ResourceGroupModelData getCurrentGroup();

    void maskGrid();

    void unMaskGrid();

    void setGroupComboEnabled(boolean enabled);

    void setRefreshEnabled(boolean hasReadRights);

    void initEditorPermissions();
    
    void selectACell();
    void deselectACell();
    
    void setCurrentCol(int col);
    
    void setCurrentRow(int row);
    int getCurrentRow();
    
    boolean withEmptyLines();
    
    DateField getMonthFilter();
    
    PersonModelData getPersonComboBoxValue();
    
    public Boolean isRadioGroupPerson();

    public void setRadioGroupPerson(Boolean bool);
    
    public void loadProjectComboBoxData();

}
